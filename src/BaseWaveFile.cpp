#include <BaseWaveFile.h>
#include "BaseWaveFile.h"

BaseWaveFile::BaseWaveFile(){
    this->RIFF = nullptr;
    this->Fmt = nullptr;
    this->Bext = nullptr;
    this->Data = nullptr;
    this->Adtl = nullptr;
    this->Plst = nullptr;
    this->Inst = nullptr;
    this->Fact = nullptr;
    this->Cue = nullptr;
    this->FileName = string();
    this->FileExtension = string(".wav");
    this->FileFullPath = string();
    this->unmappedChunks = vector<uint8_t>();
}

int BaseWaveFile::LoadFromFile(const std::string &wav_file_path){
    this->RIFF = nullptr;
    this->Fmt = nullptr;
    this->Bext = nullptr;
    this->Data = nullptr;
    this->Adtl = nullptr;
    this->Plst = nullptr;
    this->Inst = nullptr;
    this->Fact = nullptr;
    this->Cue = nullptr;
    this->unmappedChunks = vector<uint8_t>();
    this->parseFileFullPath(wav_file_path);
    std::ifstream wav_file (wav_file_path.c_str(), std::ios::in | std::ios::binary);
    if (wav_file.fail()){
        throw std::ifstream::failure("File not found!");
    }
    uint8_t * riff_type_buffer;
    riff_type_buffer = new uint8_t[12]();
    wav_file.read(reinterpret_cast<char *>(riff_type_buffer), 12);
    this->RIFF = new RiffChunk(riff_type_buffer);
    delete [] riff_type_buffer;
    uint8_t * chunk_header_buffer;
    uint8_t * chunk_buffer;
    chunk_header_buffer = new uint8_t[8]();
    uint32_t unmapped_size = 0;
    while ( ! wav_file.eof())
    {
        wav_file.read(reinterpret_cast<char *>(chunk_header_buffer), BASE_CHUNK_SIZE);
        uint32_t chunk_buffer_length;
        std::copy(chunk_header_buffer + 4, chunk_header_buffer + 8, (uint8_t *)&chunk_buffer_length);
        if (chunk_buffer_length %2 != 0){
            chunk_buffer_length += 1;
        }
        chunk_buffer = new uint8_t[chunk_buffer_length + BASE_CHUNK_SIZE]();
        std::copy(chunk_header_buffer, chunk_header_buffer + 8, chunk_buffer);
        wav_file.read(reinterpret_cast<char *>(chunk_buffer + 8), chunk_buffer_length);
        if (std::strncmp((const char *) chunk_header_buffer, "fmt ", 4) == 0)
        {
            this->Fmt = new FmtChunk(chunk_buffer);
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "cue ", 4) == 0))
        {
            this->Cue = new CueChunk(chunk_buffer);
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "data", 4) == 0))
        {
          this->Data = new DataChunk(chunk_buffer);
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "bext", 4) == 0))
        {
            this->Bext = new BextChunk(chunk_buffer);
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "fact", 4) == 0))
        {
            this->Fact = new FactChunk(chunk_buffer);
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "plst", 4) == 0))
        {
            this->Plst = new PlaylistChunk(chunk_buffer);
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "LIST", 4) == 0))
        {
            if (std::strncmp((const char *)chunk_buffer + TYPE_ID_OFFSET, "adtl", 4) == 0){
                this->Adtl = new AssociatedDataListChunk(chunk_buffer);
            }
            else{
                unmapped_size += chunk_buffer_length + BASE_CHUNK_SIZE;
                this->unmappedChunks.reserve(unmapped_size);
                this->unmappedChunks.insert(this->unmappedChunks.end(), chunk_buffer, chunk_buffer + chunk_buffer_length + BASE_CHUNK_SIZE);
            }
        }
        else if ((std::strncmp((const char *) chunk_header_buffer, "inst", 4) == 0))
        {
            this->Inst = new InstrumentChunk(chunk_buffer);
        }
        else{
            unmapped_size += chunk_buffer_length + BASE_CHUNK_SIZE;
            this->unmappedChunks.reserve(unmapped_size);
            this->unmappedChunks.insert(this->unmappedChunks.end(), chunk_buffer, chunk_buffer + chunk_buffer_length + BASE_CHUNK_SIZE);
        }
        delete [] chunk_buffer;
        std::memset((void *)chunk_header_buffer, 0, 8);
        chunk_buffer_length = 0;
    }
    delete [] chunk_header_buffer;
    wav_file.close();
    return 0;
}

void BaseWaveFile::WriteToFile(const std::string &wav_file_path){
    std::ofstream wav_file (wav_file_path.c_str(), std::ios::out | std::ios::binary);
    auto tmp = vector<uint8_t>();
    this->RIFF->getRaw(&tmp);
    wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->RIFF->getRawSize());
    wav_file.flush();
    tmp.clear();
    tmp.shrink_to_fit();
    tmp = vector<uint8_t>();
    this->Fmt->getRaw(&tmp);
    wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Fmt->getRawSize());
    wav_file.flush();
    tmp.clear();
    tmp.shrink_to_fit();
    if (this->Cue != nullptr){
        tmp = vector<uint8_t>();
        this->Cue->getRaw(&tmp);
        wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Cue->getRawSize());
        wav_file.flush();
        tmp.clear();
        tmp.shrink_to_fit();
    }
    if (this->Bext != nullptr){
        tmp = vector<uint8_t>();
        this->Bext->getRaw(&tmp);
        wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Bext->getRawSize());
        wav_file.flush();
        tmp.clear();
        tmp.shrink_to_fit();
    }
    if (this->Plst != nullptr){
        tmp = vector<uint8_t>();
        this->Plst->getRaw(&tmp);
        wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Plst->getRawSize());
        wav_file.flush();
        tmp.clear();
        tmp.shrink_to_fit();
    }
    if (this->Adtl != nullptr){
        tmp = vector<uint8_t>();
        this->Adtl->getRaw(&tmp);
        wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Adtl->getRawSize());
        wav_file.flush();
        tmp.clear();
        tmp.shrink_to_fit();
    }
    if (this->Inst != nullptr){
        tmp = vector<uint8_t>();
        this->Inst->getRaw(&tmp);
        wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Inst->getRawSize());
        wav_file.flush();
        tmp.clear();
        tmp.shrink_to_fit();
    }
    if (not this->unmappedChunks.empty()){
        wav_file.write(reinterpret_cast<const char *>(this->unmappedChunks.data()), this->unmappedChunks.size());
    }
    tmp = vector<uint8_t>();
    this->Data->getRaw(&tmp);
    wav_file.write(reinterpret_cast<const char *>(tmp.data()), this->Data->getRawSize());
    wav_file.flush();
    tmp.clear();
    tmp.shrink_to_fit();
    wav_file.flush();
    wav_file.close();
}

RiffChunk *BaseWaveFile::getRIFF() const {
    return this->RIFF;
}

FmtChunk *BaseWaveFile::getFmt() const {
    return this->Fmt;
}

CueChunk *BaseWaveFile::getCue() const {
    return this->Cue;
}

DataChunk *BaseWaveFile::getData() const {
    return this->Data;
}

void BaseWaveFile::setRIFF(RiffChunk *RIFF) {
    this->RIFF = RIFF;
}

void BaseWaveFile::setFmt(FmtChunk *Fmt) {
    BaseWaveFile::Fmt = Fmt;
}

void BaseWaveFile::setCue(CueChunk *Cue) {
    BaseWaveFile::Cue = Cue;
}

void BaseWaveFile::setData(DataChunk *Data) {
    BaseWaveFile::Data = Data;
}

void BaseWaveFile::setBext(BextChunk *Bext) {
    BaseWaveFile::Bext = Bext;
}

BextChunk *BaseWaveFile::getBext() const {
    return Bext;
}

FactChunk *BaseWaveFile::getFact() const {
    return Fact;
}

void BaseWaveFile::setFact(FactChunk *Fact) {
    BaseWaveFile::Fact = Fact;
}

PlaylistChunk *BaseWaveFile::getPlst() const {
    return Plst;
}

void BaseWaveFile::setPlst(PlaylistChunk *Plst) {
    BaseWaveFile::Plst = Plst;
}

AssociatedDataListChunk *BaseWaveFile::getAdtl() const {
    return Adtl;
}

void BaseWaveFile::setAdtl(AssociatedDataListChunk *Adtl) {
    BaseWaveFile::Adtl = Adtl;
}

InstrumentChunk *BaseWaveFile::getInst() const {
    return Inst;
}

void BaseWaveFile::setInst(InstrumentChunk *Inst) {
    BaseWaveFile::Inst = Inst;
}

bool BaseWaveFile::checkRIFFIntegrity() {
    return this->RIFF->getCkSize() == this->calculateRIFFCkSize();
}

bool BaseWaveFile::checkFile() {
    return this->checkRIFFIntegrity();
}

const string &BaseWaveFile::getFileName() const {
    return FileName;
}

void BaseWaveFile::setFileName(const string &FileName) {
    this->parseFileFullPath(FileName);
}

uint32_t BaseWaveFile::calculateRIFFCkSize() {
    uint32_t theoretical_riff_ckSize = 0;
    theoretical_riff_ckSize += this->RIFF->getRawSize() - BASE_CHUNK_SIZE;
    if (this->Cue != nullptr){
        theoretical_riff_ckSize += this->Cue->getRawSize();
    }
    if (this->Data != nullptr){
        theoretical_riff_ckSize += this->Data->getRawSize();
    }
    if (this->Fmt != nullptr){
        theoretical_riff_ckSize += this->Fmt->getRawSize();
    }
    if (this->Adtl != nullptr){
        theoretical_riff_ckSize += this->Adtl->getRawSize();
    }
    if (this->Bext != nullptr){
        theoretical_riff_ckSize += this->Bext->getRawSize();
    }
    if (this->Inst != nullptr){
        theoretical_riff_ckSize += this->Inst->getRawSize();
    }
    if (this->Plst != nullptr){
        theoretical_riff_ckSize += this->Plst->getRawSize();
    }
    if (this->Fact != nullptr){
        theoretical_riff_ckSize += this->Fact->getRawSize();
    }
    return theoretical_riff_ckSize;
}

BaseWaveFile::BaseWaveFile(const std::string &wav_file_path) {
    try{
        this->LoadFromFile(wav_file_path);
    }
    catch( const std::ifstream::failure& e ) {
       throw e;
    }
}

void BaseWaveFile::WriteToFile() {
    this->WriteToFile(this->getFileFullPath() + this->getFileExtension());
}

BaseWaveFile::~BaseWaveFile() {
    delete this->RIFF;
    delete this->Fmt;
    delete this->Bext;
    delete this->Data;
    delete this->Adtl;
    delete this->Plst;
    delete this->Inst;
    delete this->Fact;
    delete this->Cue;
}

std::ostream &operator<<(std::ostream &stream, const BaseWaveFile &chunk) {
    if (chunk.RIFF != nullptr){
        stream << *(chunk.RIFF) << endl;
    }
    if (chunk.Fmt != nullptr){
        stream << *(chunk.Fmt)<< endl;
    }
    if (chunk.Cue != nullptr){
       stream << *(chunk.Cue) << endl;
    }
    if (chunk.Adtl != nullptr){
        stream << *(chunk.Adtl) << endl;
    }
    if (chunk.Bext != nullptr){
        stream << *(chunk.Bext) << endl;
    }
    if (chunk.Inst != nullptr){
        stream << *(chunk.Inst) << endl;
    }
    if (chunk.Plst != nullptr){
        stream << *(chunk.Plst) << endl;
    }
    if (chunk.Fact != nullptr){
        stream << *(chunk.Fact) << endl;
    }
    if (chunk.Data != nullptr){
        stream << *(chunk.Data) << endl;
    }
    if (not chunk.unmappedChunks.empty()){
        stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " UNMAPPED " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl;
        if (ceil((chunk.getUnmappedChunks().size())/(MAX_PRINT_SIZE - 4)) == 0){
            stream << "| " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getUnmappedChunks().data()), (MAX_PRINT_SIZE - 4)), '*')
                   << " |" << endl;
        }
        else {
            for (uint32_t i = 0; i <= ceil((chunk.getUnmappedChunks().size()) / (MAX_PRINT_SIZE - 4)); i++) {
                stream << "| " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getUnmappedChunks().data() + i * (MAX_PRINT_SIZE - 4)),
                                                                        (MAX_PRINT_SIZE - 4)), '*')
                       << " |" << endl;
            }
        }
        stream << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;
    }
    return stream;
}

void BaseWaveFile::parseFileFullPath(const std::string &wav_file_path) {
    size_t extension_index = wav_file_path.find_last_of('.');
    size_t filename_index = wav_file_path.find_last_of('/');
    if (string::npos == extension_index){
        this->FileExtension = ".wav";
        extension_index = wav_file_path.length() + 1;
    }
    else{
        this->FileExtension = wav_file_path.substr(extension_index);
    }
    if (string::npos == filename_index){
        this->FileName =  wav_file_path.substr(0, extension_index);
    }
    else{
        this->FileName =  wav_file_path.substr(filename_index + 1, extension_index);
    }
    this->FileFullPath = wav_file_path.substr(0, extension_index);
}

const string &BaseWaveFile::getFileExtension() const {
    return FileExtension;
}

void BaseWaveFile::setFileExtension(const string &fileExtension) {
    FileExtension = fileExtension;
}

const string &BaseWaveFile::getFileFullPath() const {
    return FileFullPath;
}

void BaseWaveFile::setFileFullPath(const string &fileFullPath) {
    this->parseFileFullPath(fileFullPath);
}

RiffChunk *BaseWaveFile::getRiff() const {
    return RIFF;
}

void BaseWaveFile::setRiff(RiffChunk *riff) {
    RIFF = riff;
}

void BaseWaveFile::writeInfo() {
    auto tmp = string();
    tmp.append(this->FileFullPath);
    tmp.insert(tmp.end(), '.');
    tmp.append("info");
    std::ofstream info (tmp.c_str(), std::ios::out);
    info << *(this);
    info.flush();
    info.close();
}

const vector<uint8_t> &BaseWaveFile::getUnmappedChunks() const {
    return unmappedChunks;
}

void BaseWaveFile::setUnmappedChunks(const vector<uint8_t> &unmappedChunks) {
    this->unmappedChunks.clear();
    this->unmappedChunks.shrink_to_fit();
    this->unmappedChunks = vector<uint8_t>();
    this->unmappedChunks.reserve(unmappedChunks.size());
    this->unmappedChunks.insert(this->unmappedChunks.end(), unmappedChunks.begin(), unmappedChunks.end());
}

