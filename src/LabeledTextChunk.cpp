#include <LabeledTextChunk.h>
#include "LabeledTextChunk.h"

#define DW_SAMPLE_LENGTH_SIZE 4
#define DW_NAME_SIZE 4

uint32_t LabeledTextChunk::getDwPurpose() const {
    return dwPurpose;
}

void LabeledTextChunk::setDwPurpose(uint32_t dwPurpose) {
    LabeledTextChunk::dwPurpose = dwPurpose;
}

uint16_t LabeledTextChunk::getWCountry() const {
    return wCountry;
}

void LabeledTextChunk::setWCountry(uint16_t wCountry) {
    LabeledTextChunk::wCountry = wCountry;
}

uint16_t LabeledTextChunk::getWLanguage() const {
    return wLanguage;
}

void LabeledTextChunk::setWLanguage(uint16_t wLanguage) {
    LabeledTextChunk::wLanguage = wLanguage;
}

uint16_t LabeledTextChunk::getWDialect() const {
    return wDialect;
}

void LabeledTextChunk::setWDialect(uint16_t wDialect) {
    LabeledTextChunk::wDialect = wDialect;
}

uint16_t LabeledTextChunk::getWCodePage() const {
    return wCodePage;
}

void LabeledTextChunk::setWCodePage(uint16_t wCodePage) {
    LabeledTextChunk::wCodePage = wCodePage;
}

LabeledTextChunk::LabeledTextChunk() {
    this->setCkID((uint8_t *)&"ltxt");
    this->dwSampleLength = 0;
    this->dwPurpose = 0;
    this->wCountry = 0;
    this->wLanguage = 0;
    this->wDialect = 0;
    this->wCodePage = 0;
}

LabeledTextChunk::LabeledTextChunk(const uint8_t *RaWData) : BaseAssociatedDataListMember(RaWData) {
    this->dwSampleLength = 0;
    this->dwPurpose = 0;
    this->wCountry = 0;
    this->wLanguage = 0;
    this->wDialect = 0;
    this->wCodePage = 0;
    std::copy(RaWData + DW_SAMPLE_LENGTH_OFFSET, RaWData + DW_PURPOSE_OFFSET, (uint8_t *)&this->dwSampleLength);
    std::copy(RaWData + DW_PURPOSE_OFFSET, RaWData + W_COUNTRY_OFFSET, (uint8_t *)&this->dwPurpose);
    std::copy(RaWData + W_COUNTRY_OFFSET, RaWData + W_LANGUAGE_OFFSET, (uint8_t *)&this->wCountry);
    std::copy(RaWData + W_LANGUAGE_OFFSET, RaWData + W_DIALECT_OFFSET, (uint8_t *)&this->wLanguage);
    std::copy(RaWData + W_DIALECT_OFFSET, RaWData + W_CODE_PAG_OFFSET, (uint8_t *)&this->wDialect);
    std::copy(RaWData + W_CODE_PAG_OFFSET, RaWData + EXTENDED_DATA_OFFSET, (uint8_t *)&this->wCodePage);
}

int LabeledTextChunk::getRaw(vector<uint8_t> *raw){
    auto tmp = vector<uint8_t>();
    BaseAssociatedDataListMember::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.begin() + 4);
    raw->insert(raw->end(), (uint8_t *)&this->dwSampleLength, (uint8_t *)(&this->dwSampleLength + DW_SAMPLE_LENGTH_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->dwPurpose, (uint8_t *)(&this->dwPurpose + 4));
    raw->insert(raw->end(), (uint8_t *)&this->wCountry, (uint8_t *)(&this->wCountry + 2));
    raw->insert(raw->end(), (uint8_t *)&this->wLanguage, (uint8_t *)(&this->wLanguage + 2));
    raw->insert(raw->end(), (uint8_t *)&this->wDialect, (uint8_t *)(&this->wDialect + 2));
    raw->insert(raw->end(), (uint8_t *)&this->wCountry, (uint8_t *)(&this->wCountry + 2));
    raw->insert(raw->end(), (uint8_t *)&this->wCodePage, (uint8_t *)(&this->wCodePage + 2));
    raw->insert(raw->end(), tmp.begin() + 4, tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    return 0;

}

uint32_t LabeledTextChunk::getDwSampleLength() const {
    return dwSampleLength;
}

void LabeledTextChunk::setDwSampleLength(uint32_t dwSampleLength) {
    LabeledTextChunk::dwSampleLength = dwSampleLength;
}

std::ostream &operator<<(std::ostream &stream, const LabeledTextChunk &chunk) {
    stream  << ((BaseAssociatedDataListMember &)chunk)
            << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Sample Length "
            << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwSampleLength()) + " |  |" << endl
            << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Purpose ID "
            << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwPurpose()) + " |  |" << endl
            << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Country "
            << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getWCountry()) + " |  |" << endl
            << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Language "
            << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getWLanguage()) + " |  |" << endl
            << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Dialect "
            << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getWDialect()) + " |  |" << endl
            << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Code page "
            << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getWCodePage()) + " |  |" << endl;

    if (ceil((chunk.getCkSize() - DW_NAME_SIZE)/(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 10)) == 0) {
        stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Text "
               << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
               <<" " +  string((const char *)(chunk.getData().data()), chunk.getCkSize() - DW_NAME_SIZE) + " |  |" << endl;
    }

    else{
        stream << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Text --->"
               << setfill(' ') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " |  |" << endl;

        if (ceil((chunk.getCkSize() - DW_NAME_SIZE)/(MAX_PRINT_SIZE - 10)) == 0){
            stream << "|  | " + string((const char *) (chunk.getData().data()), (MAX_PRINT_SIZE - 10))
                   << " |  |" << endl;
        }
        else {
            for (uint32_t i = 0; i < ceil((chunk.getCkSize() - DW_NAME_SIZE) / (MAX_PRINT_SIZE - 10)); i++) {
                stream << "|  | " + string((const char *) (chunk.getData().data() + i * (MAX_PRINT_SIZE - 10)),
                                           (MAX_PRINT_SIZE - 10))
                       << " |  |" << endl;
            }
        }
    }

    stream << "|  | " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << " |  |" << endl;

    return stream;
}

LabeledTextChunk::~LabeledTextChunk() = default;
