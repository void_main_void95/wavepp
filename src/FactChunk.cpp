#include <FactChunk.h>
#include "FactChunk.h"

#define DW_SAMPLE_LENGHT_SIZE 4

uint32_t FactChunk::getDwSampleLength() const {
    return dwSampleLength;
}

void FactChunk::setDwSampleLength(uint32_t dwSampleLength) {
    FactChunk::dwSampleLength = dwSampleLength;
}

vector<uint8_t> FactChunk::getExtraBytes() const {
    return this->ExtraBytes;
}

void FactChunk::setExtraBytes(vector<uint8_t> &ExtraBytes) {
    this->ExtraBytes.clear();
    this->ExtraBytes.shrink_to_fit();
    this->ExtraBytes = vector<uint8_t>();
    this->setCkSize(DW_SAMPLE_LENGHT_SIZE + ExtraBytes.size());
    if (this->isPaddingRequired()){
        this->ExtraBytes.reserve(ExtraBytes.size() + 1);
        this->ExtraBytes.insert(this->ExtraBytes.end(), ExtraBytes.begin(), ExtraBytes.end());
        this->ExtraBytes.insert(this->ExtraBytes.end(), 0);

    }
    else{
        this->ExtraBytes.reserve(ExtraBytes.size());
        this->ExtraBytes.insert(this->ExtraBytes.end(), ExtraBytes.begin(), ExtraBytes.end());
    }
}

FactChunk::FactChunk(const uint8_t *RawData) : BaseChunk(RawData) {
    this->dwSampleLength = 0;
    std::copy(RawData + DW_SAMPLE_LENGHT_OFFSET, RawData + FACT_EXTRA_BYTES_OFFSET, (uint8_t *)&this->dwSampleLength);
    if (this->getCkSize() > DW_SAMPLE_LENGHT_SIZE){
        this->ExtraBytes = vector<uint8_t>();
        this->ExtraBytes.reserve(this->getCkSize() - DW_SAMPLE_LENGHT_SIZE);
        this->ExtraBytes.insert(this->ExtraBytes.end(), RawData + FACT_EXTRA_BYTES_OFFSET,
                RawData + getCkSize() - DW_SAMPLE_LENGHT_SIZE);
    }
}

int FactChunk::getRaw(vector<uint8_t> *raw){
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getCkSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), (uint8_t *)&this->dwSampleLength, (uint8_t *)(&this->dwSampleLength + DW_SAMPLE_LENGHT_SIZE));
    if (this->getCkSize() > DW_SAMPLE_LENGHT_SIZE){
       raw->insert(raw->end(), this->ExtraBytes.begin(), this->ExtraBytes.end());
    }
    return 0;
}

const uint32_t FactChunk::getRawSize() const{
    return BASE_CHUNK_SIZE + this->getCkSize();
}

FactChunk::FactChunk() {
    this->setCkID((uint8_t *)&"fact");
    this->setCkSize(DW_SAMPLE_LENGHT_OFFSET);
    this->dwSampleLength = 0;
    this->ExtraBytes = vector<uint8_t>();
}

std::ostream &operator<<(std::ostream &stream, const FactChunk &chunk) {
    return stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " Fact " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl
                  << ((BaseChunk &)chunk)
                  << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Extra Data "
                  << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
                  << hex << " " + string((const char *) chunk.getExtraBytes().data(), 16)+ " |" << endl
                  << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;
}

FactChunk::~FactChunk() {
    this->ExtraBytes.clear();
    this->ExtraBytes.shrink_to_fit();
}

