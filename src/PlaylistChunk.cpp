#include <PlaylistChunk.h>
#include "PlaylistChunk.h"

#define DW_SEGMENTS_SIZE 4

uint32_t PlaylistChunk::getDwSegments() const {
    return dwSegments;
}

void PlaylistChunk::setDwSegments(uint32_t dwSegments) {
    PlaylistChunk::dwSegments = dwSegments;
}

vector<PlaylistSegment *> PlaylistChunk::getPlaySegment() const {
    return this->PlaySegment;
}

void PlaylistChunk::setPlaySegment(vector<PlaylistSegment *> &PlaySegment) {
    PlaylistChunk::PlaySegment = PlaySegment;
}

PlaylistChunk::PlaylistChunk(const uint8_t *RawData) : BaseChunk(RawData) {
    this->setPaddingRequired(false);
    this->dwSegments = 0;
    std::copy(RawData + BASE_CHUNK_SIZE, RawData + DW_SEGMENTS_OFFSET, (uint8_t *)&this->dwSegments);
    uint8_t *buffer = nullptr;
    buffer = new uint8_t[PLAYLIST_SEGMENT_SIZE]();
    uint32_t n_playSegment = (this->getCkSize() - DW_SEGMENTS_SIZE)/PLAYLIST_SEGMENT_SIZE;
    this->PlaySegment = vector<PlaylistSegment *>();
    this->PlaySegment.reserve(n_playSegment);
    for(uint32_t i=0; i < n_playSegment; i++) {
        std::copy(RawData + PLAY_SEGMENT_OFFSET + PLAYLIST_SEGMENT_SIZE * i,
                RawData + PLAY_SEGMENT_OFFSET + PLAYLIST_SEGMENT_SIZE * (i + 1), buffer);
        auto *tmp = new PlaylistSegment(buffer);
        this->PlaySegment.insert(this->PlaySegment.end(), tmp);
        std::memset((void *)buffer,0, PLAYLIST_SEGMENT_SIZE);
    }
    delete [] buffer;
}

int PlaylistChunk::getRaw(vector<uint8_t >*raw){
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), (uint8_t *)&this->dwSegments, (uint8_t *)(&this->dwSegments + DW_SEGMENTS_SIZE));
    tmp = vector<uint8_t >(PLAYLIST_SEGMENT_SIZE);
    for(uint32_t i=0; i < (this->getCkSize() - DW_SEGMENTS_SIZE)/PLAYLIST_SEGMENT_SIZE; i++) {
        this->PlaySegment.at(i)->getRaw(&tmp);
        raw->insert(raw->end(), tmp.begin(), tmp.end());
        tmp.clear();
    }
    tmp.clear();
    tmp.shrink_to_fit();
    return 0;
}

const uint32_t PlaylistChunk::getRawSize() const{
    return BASE_CHUNK_SIZE + this->getCkSize();
}

PlaylistChunk::PlaylistChunk() {
    this->setCkID((uint8_t *)&"plst");
    this->setCkSize(DW_SEGMENTS_SIZE);
    this->setPaddingRequired(false);
    this->dwSegments = 0;
    this->PlaySegment = vector<PlaylistSegment *>();
}

std::ostream &operator<<(std::ostream &stream, const PlaylistChunk &chunk) {
    stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " ASSOCIATED DATA " << setw((MAX_PRINT_SIZE / 2)) << left << "LIST CHUNK " << endl
           << ((BaseChunk &)chunk)
           << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Number of segments "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
           << " " + to_string(chunk.getDwSegments()) + " |" << endl;

    stream << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
           << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;

    if (not chunk.getPlaySegment().empty()){
        for(auto &i : chunk.getPlaySegment()){
            stream << *(i)
                   << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
                   << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;
        }
    }

    stream << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;

    return stream;
}

PlaylistChunk::~PlaylistChunk() {
    for(auto &i : this->PlaySegment){
        delete i;
    }
    this->PlaySegment.clear();
    this->PlaySegment.shrink_to_fit();
}

