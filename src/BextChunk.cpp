#include <BextChunk.h>
#include "BextChunk.h"

#define DESCRIPTION_SIZE 256
#define ORIGINATOR_SIZE 32
#define ORIGINATOR_REFERENCE_SIZE 32
#define ORIGINATION_DATE_SIZE 10
#define ORIGINATION_TIME_SIZE 8
#define TIME_REFERENCE_LOW_SIZE 4
#define TIME_REFERENCE_HIGH_SIZE 4
#define VERSION_SIZE 2
#define UMID_SIZE 64
#define LOUDNESS_VALUE_SIZE 2
#define LOUDNESS_RANGE_SIZE 2
#define MAX_TRUE_PEAK_LEVEL_SIZE 2
#define MAX_MOMENTARY_LOUDNESS_SIZE 2
#define MAX_SHORT_TERM_LOUDNESS_SIZE 2
#define RESERVED_SIZE 180
#define BEXT_CHUNK_CONST_SIZE 610

BextChunk::BextChunk() {
    this->setCkID((uint8_t *)&"bext");
    this->setCkSize(0);
    this->setPaddingRequired(false);
    std::memset((void *)this->Description, 0,DESCRIPTION_SIZE);
    std::memset((void *)this->Originator, 0, ORIGINATOR_SIZE);
    std::memset((void *)this->OriginatorReference, 0, ORIGINATOR_REFERENCE_SIZE);
    std::memset((void *)this->OriginationDate, 0, ORIGINATION_DATE_SIZE);
    std::memset((void *)this->OriginationTime, 0, ORIGINATION_TIME_SIZE);
    this->TimeReferenceLow = 0;
    this->TimeReferenceHigh = 0;
    this->Version = 0;
    std::memset((void *)this->Umid, 0 , UMID_SIZE);
    this->LoudnessRange = 0;
    this->LoudnessValue = 0;
    this->MaxTruePeakLevel = 0;
    this->MaxMomentaryLoudness = 0;
    this->MaxShortTermLoudness = 0;
    std::memset((void *)this->Reserved, 0, RESERVED_SIZE);
    this->CodingHistory = vector<uint8_t>();
};

const uint8_t *BextChunk::getDescription() const {
    return this->Description;
}

const uint8_t *BextChunk::getOriginator() const {
    return this->Originator;
}

const uint8_t *BextChunk::getOriginatorReference() const {
    return this->OriginatorReference;
}

const uint8_t *BextChunk::getOriginationDate() const {
    return this->OriginationDate;
}

const uint8_t *BextChunk::getOriginationTime() const {
    return this->OriginationTime;
}

uint32_t BextChunk::getTimeReferenceLow() const {
    return this->TimeReferenceLow;
}

uint32_t BextChunk::getTimeReferenceHigh() const {
    return this->TimeReferenceHigh;
}

uint16_t BextChunk::getVersion() const {
    return this->Version;
}

const uint8_t *BextChunk::getUmid() const {
    return this->Umid;
}

uint16_t BextChunk::getLoudnessValue() const {
    return this->LoudnessValue;
}

uint16_t BextChunk::getLoudnessRange() const {
    return this->LoudnessRange;
}

uint16_t BextChunk::getMaxTruePeakLevel() const {
    return this->MaxTruePeakLevel;
}

uint16_t BextChunk::getMaxMomentaryLoudness() const {
    return this->MaxMomentaryLoudness;
}

uint16_t BextChunk::getMaxShortTermLoudness() const {
    return this->MaxShortTermLoudness;
}

const uint16_t *BextChunk::getReserved() const {
    return this->Reserved;
}

vector<uint8_t> BextChunk::getCodingHistory() const {
    return this->CodingHistory;
}

BextChunk::BextChunk(const uint8_t *RawData) : BaseChunk(RawData) {
    std::memset((void *)this->Description, 0,DESCRIPTION_SIZE);
    std::memset((void *)this->Originator, 0, ORIGINATOR_SIZE);
    std::memset((void *)this->OriginatorReference, 0, ORIGINATOR_REFERENCE_SIZE);
    std::memset((void *)this->OriginationDate, 0, ORIGINATION_DATE_SIZE);
    std::memset((void *)this->OriginationTime, 0, ORIGINATION_TIME_SIZE);
    this->TimeReferenceLow = 0;
    this->TimeReferenceHigh = 0;
    this->Version = 0;
    std::memset((void *)this->Umid, 0 , UMID_SIZE);
    this->LoudnessRange = 0;
    this->LoudnessValue = 0;
    this->MaxTruePeakLevel = 0;
    this->MaxMomentaryLoudness = 0;
    this->MaxShortTermLoudness = 0;
    std::memset((void *)this->Reserved, 0, RESERVED_SIZE);
    std::copy(RawData + DESCRIPTION_OFFSET, RawData + ORIGINATOR_OFFSET, this->Description);
    std::copy(RawData + ORIGINATOR_OFFSET, RawData + ORIGINATOR_REFERENCE_OFFSET, this->Originator);
    std::copy(RawData + ORIGINATOR_REFERENCE_OFFSET, RawData + ORIGINATION_DATE_OFFSET, this->OriginatorReference);
    std::copy(RawData + ORIGINATION_DATE_OFFSET, RawData + ORIGINATION_TIME_OFFSET, this->OriginationDate);
    std::copy(RawData + ORIGINATION_TIME_OFFSET, RawData + TIME_REFERENCE_LOW_OFFSET, this->OriginationTime);
    std::copy(RawData + TIME_REFERENCE_LOW_OFFSET, RawData + TIME_REFERENCE_LOW_OFFSET, (uint8_t *)&this->TimeReferenceLow);
    std::copy(RawData + TIME_REFERENCE_HIGH_OFFSET, RawData + VERSION_OFFSET, (uint8_t *)&this->TimeReferenceHigh);
    std::copy(RawData + VERSION_OFFSET, RawData + UMID_OFFSET, (uint8_t *)&this->Version);
    std::copy(RawData + UMID_OFFSET, RawData + LOUDNESS_VALUE_OFFSET, this->Umid);
    std::copy(RawData + LOUDNESS_VALUE_OFFSET, RawData + LOUDNESS_RANGE_OFFSET, (uint8_t *)&this->LoudnessValue);
    std::copy(RawData + LOUDNESS_RANGE_OFFSET, RawData + MAX_TRUE_PEAK_LEVEL_OFFSET, (uint8_t *)&this->LoudnessRange);
    std::copy(RawData + MAX_TRUE_PEAK_LEVEL_OFFSET, RawData + MAX_MOMENTARY_LOUDNESS_OFFSET, (uint8_t *)&this->MaxTruePeakLevel);
    std::copy(RawData + MAX_MOMENTARY_LOUDNESS_OFFSET, RawData + MAX_SHORT_TERM_LOUDNESS_OFFSET, (uint8_t *)&this->MaxMomentaryLoudness);
    std::copy(RawData + MAX_MOMENTARY_LOUDNESS_OFFSET, RawData + RESERVED_OFFSET, (uint8_t *)&this->MaxShortTermLoudness);
    std::copy(RawData + RESERVED_OFFSET, RawData + CODING_HISTORY_OFFSET, this->Reserved);
    this->CodingHistory = vector<uint8_t>();
    if (this->isPaddingRequired()){
        this->CodingHistory.reserve(this->getCkSize() - BEXT_CHUNK_CONST_SIZE + 1);
        this->CodingHistory.insert(this->CodingHistory.end(), RawData + CODING_HISTORY_OFFSET,
                RawData + this->getCkSize() + 1);
    }
    else {
        this->CodingHistory.reserve((this->getCkSize() - BEXT_CHUNK_CONST_SIZE));
        this->CodingHistory.insert(this->CodingHistory.end(), RawData + CODING_HISTORY_OFFSET,
                                   RawData + this->getCkSize());
    }
}

int BextChunk::getRaw(vector<uint8_t> * raw) {
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->reserve(this->getCkSize());
    raw->insert(raw->end(),this->Description, this->Description + DESCRIPTION_SIZE);
    raw->insert(raw->end(),this->Originator, this->Originator + ORIGINATOR_SIZE);
    raw->insert(raw->end(),this->OriginatorReference, this->OriginatorReference + ORIGINATOR_REFERENCE_SIZE);
    raw->insert(raw->end(), this->OriginationDate, this->OriginationDate + ORIGINATION_DATE_SIZE);
    raw->insert(raw->end(), this->OriginationTime, this->OriginationTime + ORIGINATION_TIME_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->TimeReferenceLow, (uint8_t *)(&this->TimeReferenceLow + TIME_REFERENCE_LOW_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->TimeReferenceHigh, (uint8_t *)(&this->TimeReferenceHigh + TIME_REFERENCE_HIGH_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->Version, (uint8_t *)(&this->Version + VERSION_SIZE));
    raw->insert(raw->end(), this->Umid, this->Umid + UMID_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->LoudnessValue, (uint8_t *)(&this->LoudnessValue + LOUDNESS_VALUE_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->LoudnessRange, (uint8_t *)(&this->LoudnessRange + LOUDNESS_RANGE_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->MaxTruePeakLevel, (uint8_t *)(&this->MaxTruePeakLevel + MAX_TRUE_PEAK_LEVEL_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->MaxMomentaryLoudness, (uint8_t *)(&this->MaxMomentaryLoudness + MAX_MOMENTARY_LOUDNESS_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->MaxShortTermLoudness, (uint8_t *)(&this->MaxShortTermLoudness + MAX_SHORT_TERM_LOUDNESS_SIZE));
    raw->insert(raw->end(),this->Reserved, this->Reserved + RESERVED_SIZE);
    raw->insert(raw->end(), this->CodingHistory.begin(), this->CodingHistory.end());
    return 0;
}

const uint32_t BextChunk::getRawSize() const {
    if (this->isPaddingRequired()){
        return BASE_CHUNK_SIZE + this->getCkSize() + 1;
    }
    return BASE_CHUNK_SIZE + this->getCkSize() ;
}

BextChunk::~BextChunk(){
    this->CodingHistory.clear();
    this->CodingHistory.shrink_to_fit();
}

std::ostream &operator<<(std::ostream &stream, const BextChunk &chunk) {
    stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " BEXT" << setw((MAX_PRINT_SIZE / 2)) << left << " CHUNK " << endl
                << ((BaseChunk &)chunk)
                << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Description "
                << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
                << " " + string((const char *)chunk.getDescription(), MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3) + " |" << endl;
    for (uint32_t i = 0; i <= ceil((256 - (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3)) / (MAX_PRINT_SIZE - 4)); i++) {
        stream << setfill('.') << "| " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getDescription() +
        (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3) + i * (MAX_PRINT_SIZE - 4)),
                                   (MAX_PRINT_SIZE - 4)), '*')
               << " |" << endl;
    }
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Originator "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
                    << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getOriginator(),
                            MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3), '*') + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Originator reference "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
           << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getOriginatorReference(),
                   MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3), '*') + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Origination date "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
           << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getOriginationDate(), MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3), '*') + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Origination time "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
           << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getOriginationTime(), MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3), '*') + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Time reference low "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getTimeReferenceLow()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Time reference high "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getTimeReferenceHigh()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Version "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getVersion()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Umid "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
                    << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getUmid(),
                            MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3),'*') + " |" << endl;
    stream << "| " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getUmid() +
    (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3) + 64), MAX_PRINT_SIZE - 4), '*') + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Loudness value "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getLoudnessValue()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Loudness range "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getLoudnessRange()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Max True peak level "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getMaxTruePeakLevel()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Max momentary loudness "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getMaxMomentaryLoudness()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Max short term loudness "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getMaxShortTermLoudness()) + " |" << endl;
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Reserved "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
           << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getReserved(), MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3), '*') + " |" << endl;
    for (uint32_t i = 0; i <= ceil((180 - (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3)) / (MAX_PRINT_SIZE - 4)); i++) {
        stream << "| " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getReserved() + (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3)
        + i * (MAX_PRINT_SIZE - 4)), (MAX_PRINT_SIZE - 4)),'*')
               << " |" << endl;
    }
    stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Coding history "
           << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
           << " " + BaseChunk::stripNonPrintable(string((const char *)chunk.getCodingHistory().data(), MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3), '*')
           + " |" << endl;
    for (uint32_t i = 0; i <= ceil((chunk.getCodingHistory().size() - (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3)) / (MAX_PRINT_SIZE - 4)); i++) {
        stream << "| " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getReserved() + (MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 3)
        + i * (MAX_PRINT_SIZE - 4)),(MAX_PRINT_SIZE - 4)), '*')
               << " |" << endl;
    }
    stream << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;
    return stream;
}
