#include <FmtChunk.h>
#include "FmtChunk.h"

#define W_FORMAT_TAG_SIZE 2
#define N_CHANNELS_SIZE 2
#define N_SAMPLES_PER_SEC_SIZE 4
#define N_AVG_BYTES_PER_SEC_SIZE 4
#define N_BLOCK_ALIGN_SIZE 2
#define W_BITS_PER_SAMPLES_SIZE 2
#define CB_SIZE_SIZE 2
#define DEFAULT_FMT_CK_SIZE 16
#define DEFAULT_EXTENDEND_FMT_CK_SIZE 18

FmtChunk::FmtChunk(const uint8_t *RaWData) : BaseChunk(RaWData) {
    this->wFormatTag = 0;
    this->nChannels = 0;
    this->nSamplesPerSec = 0;
    this->nAvgBytesPerSec = 0;
    this->nBlockAlign = 0;
    this->wBitsPerSample = 0;
    this->cbSize = 0;
    std::copy(RaWData + W_FORMAT_TAG_OFFSET, RaWData + N_CHANNELS_OFFSET, (uint8_t *)&(this->wFormatTag));
    std::copy(RaWData + N_CHANNELS_OFFSET, RaWData + N_SAMPLES_PER_SEC_OFFSET, (uint8_t *)&(this->nChannels));
    std::copy(RaWData + N_SAMPLES_PER_SEC_OFFSET, RaWData + N_AVG_BYTES_PER_SEC_OFFSET, (uint8_t *)&(this->nSamplesPerSec));
    std::copy(RaWData + N_AVG_BYTES_PER_SEC_OFFSET, RaWData + N_BLOCK_ALIGN_OFFSET, (uint8_t *)&(this->nAvgBytesPerSec));
    std::copy(RaWData + N_BLOCK_ALIGN_OFFSET, RaWData + W_BITS_PER_SAMPLES_OFFSET, (uint8_t *)&(this->nBlockAlign));
    std::copy(RaWData + W_BITS_PER_SAMPLES_OFFSET, RaWData + CB_SIZE_OFFSET, (uint8_t *)&(this->wBitsPerSample));
    if (this->getCkSize() > DEFAULT_FMT_CK_SIZE){
        std::copy(RaWData + CB_SIZE_OFFSET, RaWData + FMT_EXTRA_BYTES_OFFSET, (uint8_t *)&(this->cbSize));
        if (this->cbSize != (uint16_t)(0)) {
            this->ExtraBytes = vector<uint8_t>();
            if (this->isPaddingRequired()){
                this->ExtraBytes.reserve(this->cbSize + 1);
                this->ExtraBytes.insert(this->ExtraBytes.end(), RaWData + FMT_EXTRA_BYTES_OFFSET,
                        RaWData + FMT_EXTRA_BYTES_OFFSET + this->cbSize + 1);
            }
            else{
                this->ExtraBytes.reserve(this->cbSize);
                this->ExtraBytes.insert(this->ExtraBytes.end(), RaWData + FMT_EXTRA_BYTES_OFFSET,
                                        RaWData + FMT_EXTRA_BYTES_OFFSET + this->cbSize);
            }
        }
    }
    else{
        this->ExtraBytes = vector<uint8_t>();
    }
}

int FmtChunk::getRaw(vector<uint8_t> *raw) {
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), (uint8_t *)&this->wFormatTag, (uint8_t *)(&this->wFormatTag) + W_FORMAT_TAG_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->nChannels, (uint8_t *)(&this->nChannels) + N_CHANNELS_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->nSamplesPerSec, (uint8_t *)(&this->nSamplesPerSec) + N_SAMPLES_PER_SEC_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->nAvgBytesPerSec, (uint8_t *)(&this->nAvgBytesPerSec) + N_AVG_BYTES_PER_SEC_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->nBlockAlign, (uint8_t *)(&this->nBlockAlign) + N_BLOCK_ALIGN_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->wBitsPerSample, (uint8_t *)(&this->wBitsPerSample) + W_BITS_PER_SAMPLES_SIZE);
    if (this->getCkSize() > DEFAULT_FMT_CK_SIZE){
        raw->insert(raw->end(), (uint8_t *)&this->cbSize, (uint8_t *)(&this->cbSize) + CB_SIZE_SIZE);
        if (this->cbSize != (uint16_t)(0)) {
            raw->insert(raw->end(), this->ExtraBytes.begin(), this->ExtraBytes.end());
        }
    }
    return 0;
}

const uint32_t FmtChunk::getRawSize() const{
    if (this->isPaddingRequired()){
        return BASE_CHUNK_SIZE + getCkSize() + 1;
    }
    return BASE_CHUNK_SIZE + getCkSize();
}

uint16_t FmtChunk::getWFormatTag() const {
    return this->wFormatTag;
}

uint16_t FmtChunk::getNChannels() const {
    return this->nChannels;
}

uint32_t FmtChunk::getNSamplesPerSec() const {
    return this->nSamplesPerSec;
}

uint32_t FmtChunk::getNAvgBytesPerSec() const {
    return this->nAvgBytesPerSec;
}

uint16_t FmtChunk::getNBlockAlign() const {
    return this->nBlockAlign;
}

uint16_t FmtChunk::getWBitsPerSample() const {
    return this->wBitsPerSample;
}

uint16_t FmtChunk::getCbSize() const {
    return this->cbSize;
}

vector<uint8_t> FmtChunk::getExtraBytes() const {
    return this->ExtraBytes;
}

void FmtChunk::setWFormatTag(uint16_t wFormatTag) {
    this->wFormatTag = wFormatTag;
}

void FmtChunk::setNChannels(uint16_t nChannels) {
    this->nChannels = nChannels;
}

void FmtChunk::setNSamplesPerSec(uint32_t nSamplesPerSec) {
    this->nSamplesPerSec = nSamplesPerSec;
}

void FmtChunk::setNAvgBytesPerSec(uint32_t nAvgBytesPerSec) {
    this->nAvgBytesPerSec = nAvgBytesPerSec;
}

void FmtChunk::setNBlockAlign(uint16_t nBlockAlign) {
    this->nBlockAlign = nBlockAlign;
}

void FmtChunk::setWBitsPerSample(uint16_t wBitsPerSample) {
    this->wBitsPerSample = wBitsPerSample;
}

void FmtChunk::setCbSize(uint16_t cbSize) {
    this->cbSize = cbSize;
}

void FmtChunk::setExtraBytes(vector<uint8_t> &ExtraBytes) {
    this->ExtraBytes.clear();
    this->ExtraBytes.shrink_to_fit();
    this->ExtraBytes = vector<uint8_t>();
    this->cbSize = ExtraBytes.size();
    this->setCkSize(DEFAULT_EXTENDEND_FMT_CK_SIZE + ExtraBytes.size());
    if (this->isPaddingRequired()){
        this->ExtraBytes.reserve(ExtraBytes.size() + 1);
        this->ExtraBytes.insert(this->ExtraBytes.end(), ExtraBytes.begin(), ExtraBytes.end());
        this->ExtraBytes.insert(this->ExtraBytes.end(), 0);
    }
    else{
        this->ExtraBytes.reserve(ExtraBytes.size());
        this->ExtraBytes.insert(this->ExtraBytes.end(), ExtraBytes.begin(), ExtraBytes.end());
    }


}

FmtChunk::FmtChunk() : BaseChunk(){
    this->setCkID((uint8_t *)&"fmt ");
    this->setCkSize(16);
    this->wFormatTag = WAVE_FORMAT_UNKNOWN;
    this->nChannels = 0;
    this->nSamplesPerSec = 0;
    this->nAvgBytesPerSec = 0;
    this->nBlockAlign = 0;
    this->wBitsPerSample = 0;
    this->cbSize = 0;
    this->ExtraBytes = vector<uint8_t>();
}

std::ostream &operator<<(std::ostream &stream, const FmtChunk &chunk) {
    stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " FMT " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl
    << ((BaseChunk &)chunk)
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Format tag "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getWFormatTag()) + " |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Number of channels "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getNChannels()) + " |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Samples per second "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getNSamplesPerSec()) + " |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left <<"| Bytes per second "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getNAvgBytesPerSec()) + " |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Block align "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getNBlockAlign()) + " |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left <<"| Bits per sample "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getWBitsPerSample()) + " |" << endl;
    if (chunk.getCkSize() > 16){
        stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left <<"| Extra format bytes "
               << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getCbSize()) + " |" << endl;
        if (chunk.getCbSize() != 0){
            stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left <<"| Extra format bytes "
                   << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << hex <<
                   " " +  BaseChunk::stripNonPrintable(string((const char *) chunk.getExtraBytes().data(), 16),'*') + " |" << endl;
        }
    }

    stream << setfill('-') << setw(MAX_PRINT_SIZE) << '-' << endl
    << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;

    return stream;
}

FmtChunk::~FmtChunk()  {
    this->ExtraBytes.clear();
    this->ExtraBytes.shrink_to_fit();
}



