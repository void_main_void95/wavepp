#include <BaseAssociatedDataListMember.h>
#include "BaseAssociatedDataListMember.h"

#define DW_NAME_SIZE 4
#define CK_ID_SIZE 4

uint32_t BaseAssociatedDataListMember::getDwName() const {
    return dwName;
}

void BaseAssociatedDataListMember::setDwName(uint32_t dwName) {
    BaseAssociatedDataListMember::dwName = dwName;
}

vector<uint8_t> BaseAssociatedDataListMember::getData() const {
    return data;
}

void BaseAssociatedDataListMember::setData(std::vector<uint8_t> &data) {
    BaseAssociatedDataListMember::data = data;
}

BaseAssociatedDataListMember::BaseAssociatedDataListMember() {
    this->setCkID((uint8_t *)&"base");
    this->setCkSize(4);
    this->dwName = 0;
    this->data = vector<uint8_t>();
}

BaseAssociatedDataListMember::BaseAssociatedDataListMember(const uint8_t *RaWData) : BaseChunk(RaWData) {
    this->dwName = 0;
    std::copy(RaWData + BASE_DW_NAME_OFFSET, RaWData + BASE_DATA_OFFSET, (uint8_t *)&this->dwName);
    this->data = vector<uint8_t>();
    if (std::strncmp((const char *)this->getCkID(), "ltxt", 4) == 0){
        if (this->isPaddingRequired()){
            this->data.reserve(this->getCkSize() - DW_NAME_SIZE + 1);
            this->data.insert(this->data.end(), RaWData + EXTENDED_DATA_OFFSET,
                              RaWData + EXTENDED_DATA_OFFSET + this->getCkSize() - EXTENDED_SIZE - DW_NAME_SIZE + 1);
        }
        else {
            this->data.reserve(this->getCkSize() - DW_NAME_SIZE);
            this->data.insert(this->data.end(), RaWData + EXTENDED_DATA_OFFSET,
                              RaWData + EXTENDED_DATA_OFFSET + this->getCkSize() - EXTENDED_SIZE - DW_NAME_SIZE);
        }
    }
    else {
        if (this->isPaddingRequired()){
            this->data.reserve(this->getCkSize() - DW_NAME_SIZE + 1);
            this->data.insert(this->data.end(),RaWData + BASE_DATA_OFFSET,
                   RaWData + BASE_DATA_OFFSET + this->getCkSize() - DW_NAME_SIZE + 1);
        }
        else{
            this->data.reserve(this->getCkSize() - DW_NAME_SIZE);
            this->data.insert(this->data.end(),RaWData + BASE_DATA_OFFSET,
                              RaWData + BASE_DATA_OFFSET + this->getCkSize() - DW_NAME_SIZE);
        }
    }
}

int BaseAssociatedDataListMember::getRaw(vector<uint8_t> *raw){
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), (uint8_t *)&this->dwName, (uint8_t *)(&this->dwName + DW_NAME_SIZE));
    raw->insert(raw->end(), this->data.begin(), this->data.end());
    return 0;
}

const uint32_t BaseAssociatedDataListMember::getRawSize() const{
    if (this->isPaddingRequired()){
        return BASE_CHUNK_SIZE + this->getCkSize() + 1;
    }
    return BASE_CHUNK_SIZE + this->getCkSize();
}

std::ostream &operator<<(std::ostream &stream, const BaseAssociatedDataListMember &chunk) {
    return stream   << "|  | " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << " |  |" << endl
                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Chunk ID "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
                    << " " + string((const char *)chunk.getCkID(), CK_ID_SIZE) + " |  |" << endl
                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "|  | Chunk size "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getCkSize()) + " |  |" << endl
                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Cue point ID "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
                    << " " +  to_string(chunk.getDwName()) + " |  |" << endl;
}

BaseAssociatedDataListMember::~BaseAssociatedDataListMember() {
    this->data.clear();
    this->data.shrink_to_fit();
}
