
#include <BaseChunk.h>

#include "BaseChunk.h"

#define CK_ID_SIZE 4
#define CK_SIZE_SIZE 4

BaseChunk::BaseChunk() {
    this->setCkID((uint8_t *)&"none");
    this->setCkSize(0);
    this->paddingRequired = false;
};

BaseChunk::BaseChunk(const uint8_t * RaWData) {
    std::copy(RaWData + CK_ID_OFFSET, RaWData + CK_SIZE_OFFSET, this->ckID);
    std::copy(RaWData + CK_SIZE_OFFSET, RaWData + BASE_CHUNK_SIZE, (uint8_t *)&this->ckSize);
    this->paddingRequired = this->ckSize % 2 != 0;
}

int BaseChunk::getRaw(vector<uint8_t> *raw) {
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(),this->ckID, this->ckID + CK_ID_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->ckSize, (uint8_t *)(&this->ckSize) + CK_SIZE_SIZE);
    return 0;
}

const uint8_t *BaseChunk::getCkID() const {
    return this->ckID;
}

uint32_t BaseChunk::getCkSize() const {
    return this->ckSize;
}

void BaseChunk::setCkSize(uint32_t ckSize) {
    this->paddingRequired = ckSize % 2 != 0;
    this->ckSize = ckSize;
}

void BaseChunk::setCkID(uint8_t * ckID){
    std::copy(ckID, ckID + 4, this->ckID);
}

const uint32_t BaseChunk::getRawSize() const{
    return BASE_CHUNK_SIZE;
}

bool BaseChunk::isPaddingRequired() const {
    return paddingRequired;
}

void BaseChunk::setPaddingRequired(bool paddingRequired) {
    this->paddingRequired = paddingRequired;
}

std::ostream &operator<<(std::ostream &stream, const BaseChunk &chunk) {
    return stream   << "| " << setfill('-') << setw((MAX_PRINT_SIZE - 4) / 2) << right << " chunk "
                    << setw((MAX_PRINT_SIZE - 4) / 2) << left << "header " << " |" << endl
                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Chunk ID "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
                    << " " + string((const char *)chunk.getCkID(), CK_ID_SIZE) + " |" << endl
                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) <<  left << "| Chunk size "
                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getCkSize()) + " |" << endl
                    <<  "| " << setfill('-') << setw((MAX_PRINT_SIZE - 4) / 2) << right << " chunk "
                    << setw((MAX_PRINT_SIZE - 4) / 2) << left << "data " << " |"<< endl;
}

const string  BaseChunk::stripNonPrintable(const string &NonPrintableString, char filler='.') {
    auto printable = string();
    for(auto &i : NonPrintableString){
        if (not isprint(i)){
            printable += filler;
        }
        else{
            printable += i;
        }
    }
    return printable;
}

BaseChunk::~BaseChunk() = default;