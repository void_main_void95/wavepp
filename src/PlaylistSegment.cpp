#include <PlaylistSegment.h>
#include "PlaylistSegment.h"

#define DW_NAME_SIZE 4
#define DW_LENGTH_SIZE 4
#define DW_LOOPS_SIZE 4

uint32_t PlaylistSegment::getDwName() const {
    return dwName;
}

uint32_t PlaylistSegment::getDwLength() const {
    return dwLength;
}

uint32_t PlaylistSegment::getDwLoops() const {
    return dwLoops;
}

void PlaylistSegment::setDwName(uint32_t dwName) {
    PlaylistSegment::dwName = dwName;
}

void PlaylistSegment::setDwLength(uint32_t dwLength) {
    PlaylistSegment::dwLength = dwLength;
}

void PlaylistSegment::setDwLoops(uint32_t dwLoops) {
    PlaylistSegment::dwLoops = dwLoops;
}

PlaylistSegment::PlaylistSegment(const uint8_t *RawData) {
    std::copy(RawData + DW_NAME_OFFSET, RawData + DW_LENGTH_OFFSET, (uint8_t *)&this->dwName);
    std::copy(RawData + DW_LENGTH_OFFSET, RawData + DW_LOOPS_OFFSET, (uint8_t *)&this->dwLength);
    std::copy(RawData + DW_LOOPS_OFFSET, RawData + PLAYLIST_SEGMENT_SIZE, (uint8_t *)&this->dwLoops);
}

int PlaylistSegment::getRaw(vector<uint8_t >* raw) {
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), (uint8_t *)&this->dwName, (uint8_t *)(&this->dwName + DW_NAME_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->dwLength, (uint8_t *)(&this->dwLength + DW_LENGTH_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->dwLoops, (uint8_t *)(&this->dwLoops + DW_LOOPS_SIZE));
    return 0;
}

const uint32_t PlaylistSegment::getRawSize() const{
    return PLAYLIST_SEGMENT_SIZE;
}

std::ostream &operator<<(std::ostream &stream, const PlaylistSegment &chunk) {
    return stream << "|    " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << "    |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Cue point ID "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwName()) + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Length "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwLength()) + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Number of Repeats "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwLoops()) + " |  |" << endl
    << "|    " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << "    |" << endl;
}

PlaylistSegment::~PlaylistSegment() = default;


PlaylistSegment::PlaylistSegment() {
    this->dwName = 0;
    this->dwLength = 0;
    this->dwLength = 0;

}
