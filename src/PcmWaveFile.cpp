#include <PcmWaveFile.h>
#include "PcmWaveFile.h"

int PcmWaveFile::getPCMLeftChannel(std::vector<uint8_t> *out_channel){
    if ((this->getFmt()->getNChannels() < 2) or (this->getFmt()->getWFormatTag() != WAVE_FORMAT_PCM)){
        return 1;
    }

    uint16_t block_size = this->getFmt()->getWBitsPerSample() * this->getFmt()->getNChannels() / (uint16_t )(8);
    uint32_t n_blocks = this->Data->getCkSize() / block_size;
    for(uint32_t i=0; i < n_blocks; i++){
        out_channel->insert(out_channel->end(), this->Data->getCkData().data() + (i * block_size),
                this->Data->getCkData().data()+ (block_size / this->getFmt()->getNChannels()) + (i* block_size));
    }
    return 0;
}

uint32_t PcmWaveFile::getPCMSingleChannelSize(){
    if (this->getFmt()->getNChannels() < 2){
        return 0;
    }
    return this->Data->getCkSize() / 2;
}

int PcmWaveFile::getPCMRightChannel(std::vector<uint8_t> *out_channel){
    if ((this->getFmt()->getNChannels() < 2) or (this->getFmt()->getWFormatTag() != WAVE_FORMAT_PCM)){
        return 1;
    }
    uint16_t block_size = (this->getFmt()->getWBitsPerSample() * this->getFmt()->getNChannels()) / (uint16_t )(8);
    uint32_t n_blocks = this->Data->getCkSize() / block_size;
    uint32_t right_offset = 0;
    switch (this->getFmt()->getWBitsPerSample()){
        case 8:
            right_offset = 1;
            break;
        case 16:
            right_offset = 2;
            break;
        case 24:
            right_offset = 3;
            break;
        case 32:
            right_offset = 4;
            break;
        default:
            right_offset = 1;
            break;
    };
    for(uint32_t i=0; i < n_blocks; i++) {
        out_channel->insert(out_channel->end(), this->Data->getCkData().data() + (i * block_size) + right_offset,
                  this->Data->getCkData().data() + right_offset + (block_size / this->getFmt()->getNChannels()) + (i * block_size));
    }
    return 0;
}

int  PcmWaveFile::pcmStereoToMono(std::vector<uint8_t> *out_channel, float left_channel_weight, float right_channel_weight){
    if (left_channel_weight == 0.0 and right_channel_weight == 1.0){
        this->getPCMRightChannel(out_channel);
        cout << out_channel->size();
        return 0;
    }
    if (left_channel_weight == 1.0 and right_channel_weight == 0.0){
        this->getPCMLeftChannel(out_channel);
        return 0;
    }
    auto left_channel = vector<uint8_t>();
    left_channel.reserve(this->getPCMSingleChannelSize());
    this->getPCMLeftChannel(&left_channel);
    auto right_channel = vector<uint8_t>();
    this->getPCMRightChannel(&right_channel);
    right_channel.reserve(this->getPCMSingleChannelSize());
    if (not left_channel.empty() and not right_channel.empty()){
        PcmWaveFile::MergeChannels(out_channel, &left_channel, &right_channel,
                                            this->getFmt()->getWBitsPerSample(),
                                            this->getFmt()->getWBitsPerSample(),
                                            this->getFmt()->getWBitsPerSample(),
                                            this->getPCMSingleChannelSize(),
                                            left_channel_weight,
                                            right_channel_weight);
        left_channel.clear();
        left_channel.shrink_to_fit();
        right_channel.clear();
        right_channel.shrink_to_fit();
        return 0;
        
    }
    return 1;
}

bool PcmWaveFile::checkFmtIntegrity() {
    return (this->getFmt()->getNBlockAlign() == ((this->getFmt()->getWBitsPerSample() / (uint16_t )(8)) *
                                                 this->getFmt()->getNChannels())) and
           (this->getFmt()->getNAvgBytesPerSec() == (this->getFmt()->getNSamplesPerSec() * this->getFmt()->getNBlockAlign()));
}

bool PcmWaveFile::checkFile() {
    return BaseWaveFile::checkFile() and this->checkFmtIntegrity();
}

PcmWaveFile::PcmWaveFile() : BaseWaveFile(){
}

int
PcmWaveFile::MergeChannels(std::vector<uint8_t >* out_channel, std::vector<uint8_t >* channel_1, std::vector<uint8_t >* channel_2,
                           uint16_t channel_1_WBitsPerSample,
                           uint16_t channel_2_WBitsPerSample,
                           uint16_t merged_WBitsPerSample,
                           uint32_t merged_size,
                           float channel_1_weight,
                           float channel_2_weight) {
    if ( (channel_1_WBitsPerSample != merged_WBitsPerSample or channel_2_WBitsPerSample != merged_WBitsPerSample)
    and (merged_WBitsPerSample > 16)){
        return 1;
    }
    uint32_t channel_1_bytes_per_sample = channel_1_WBitsPerSample/(uint32_t )(8);
    uint32_t  channel_2_bytes_per_sample = channel_2_WBitsPerSample/(uint32_t)(8);
    uint32_t merged_bytes_per_sample = merged_WBitsPerSample / (uint32_t)(8);
    uint32_t channel_1_n_blocks = ((uint32_t )channel_1->size() / channel_1_bytes_per_sample);
    uint32_t channel_2_n_blocks = ((uint32_t )channel_2->size() / channel_2_bytes_per_sample);
    uint32_t merged_n_blocks = merged_size / ( (merged_WBitsPerSample) / 8);
    for(uint32_t i=0; i < merged_n_blocks; i++){
        if (merged_WBitsPerSample == 16){
            int16_t channel_1_sample = 0;
            int16_t channel_2_sample = 0;
            int16_t merged_sample = 0;
            if (i < channel_1_n_blocks){
                        std::copy(channel_1->data() + (i * channel_1_bytes_per_sample),
                                  channel_1->data() + channel_1_bytes_per_sample * (i  + 1),
                                  (uint8_t *)&channel_1_sample);
            }
            else{
                uint32_t j;
                j = (i - (channel_1_n_blocks*(uint32_t)(i / channel_1_n_blocks)));
                std::copy(channel_1->data() + (j * channel_1_bytes_per_sample),
                          channel_1->data() + channel_1_bytes_per_sample * (j + 1),
                          (uint8_t *)&channel_1_sample);
            }
            if ( i < channel_2_n_blocks){
                std::copy(channel_2->data() + (i * channel_2_bytes_per_sample),
                          channel_2->data() + channel_2_bytes_per_sample * (i + 1),
                          (uint8_t *)&channel_2_sample);
            } else{
                uint32_t j;
                j = (i - (channel_2_n_blocks*(uint32_t)(i / channel_2_n_blocks)));
                std::copy(channel_2->data() + (j * channel_2_bytes_per_sample),
                          channel_2->data() + channel_2_bytes_per_sample * (j + 1),
                          (uint8_t *)&channel_2_sample);
            }
            merged_sample = (int16_t)(channel_1_weight * channel_1_sample + channel_2_weight * channel_2_sample);
            out_channel->insert(out_channel->end(),(uint8_t *)&merged_sample, (uint8_t *)(&merged_sample) + merged_bytes_per_sample);
        }
        else if(merged_WBitsPerSample == 8) {
            uint8_t channel_1_sample = 0;
            uint8_t channel_2_sample = 0;
            uint8_t merged_sample = 0;
            if (i < channel_1_n_blocks){
                std::copy(channel_1->data() + (i * channel_1_bytes_per_sample),
                          channel_1->data() + channel_1_bytes_per_sample * (i  + 1),
                          &channel_1_sample);
            }
            else{
                uint32_t j;
                j = (i - (channel_1_n_blocks*(uint32_t)(i / channel_1_n_blocks)));
                std::copy(channel_1->data() + (j * channel_1_bytes_per_sample),
                          channel_1->data() + channel_1_bytes_per_sample * (j + 1),
                          &channel_1_sample);
            }
            if ( i < channel_2_n_blocks){
                std::copy(channel_2->data() + (i * channel_2_bytes_per_sample),
                          channel_2->data() + channel_2_bytes_per_sample * (i + 1),
                          &channel_2_sample);
            } else{
                uint32_t j;
                j = (i - (channel_2_n_blocks*(uint32_t)(i / channel_2_n_blocks)));
                std::copy(channel_2->data() + (j * channel_2_bytes_per_sample),
                          channel_2->data() + channel_2_bytes_per_sample * (j + 1),
                          &channel_2_sample);
            }
            merged_sample = (uint8_t) (channel_1_weight * channel_1_sample + channel_2_weight * channel_2_sample);
            out_channel->insert(out_channel->end(),(uint8_t *)&merged_sample, (uint8_t *)(&merged_sample) + merged_bytes_per_sample);
        }
    }
    return 0;
}

void PcmWaveFile::checkAndRestore() {
    if (! this->checkRIFFIntegrity()){
        this->getRIFF()->setCkSize(this->calculateRIFFCkSize());
    }
    if (! this->checkFmtIntegrity()){
        this->getFmt()->setNBlockAlign(((this->getFmt()->getWBitsPerSample() / (uint16_t )(8)) *
                                        this->getFmt()->getNChannels()));
        this->getFmt()->setNAvgBytesPerSec(this->getFmt()->getNSamplesPerSec() * this->getFmt()->getNBlockAlign());
    }
}



PcmWaveFile::~PcmWaveFile(){
}

PcmWaveFile::PcmWaveFile(const std::string &wav_file_path) : BaseWaveFile(wav_file_path) {

}

