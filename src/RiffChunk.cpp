#include <RiffChunk.h>
#include "RiffChunk.h"

#define WAVE_ID_SIZE  4

RiffChunk::RiffChunk() : BaseChunk(){
    this->setCkID((uint8_t *)&"RIFF");
    this->setCkSize(WAVE_ID_SIZE);
    this->setRiffType((uint8_t *)&"WAVE");
}

RiffChunk::RiffChunk(const uint8_t *RaWData) : BaseChunk(RaWData) {
    std::copy(RaWData + WAVE_ID_OFFSET, RaWData + WAVE_ID_OFFSET + WAVE_ID_SIZE, this->RiffType);
}

int RiffChunk::getRaw(std::vector<uint8_t> *raw) {
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    raw->insert(raw->end(), this->RiffType, this->RiffType + WAVE_ID_SIZE);
    tmp.clear();
    tmp.shrink_to_fit();
    return 0;
}

const uint32_t RiffChunk::getRawSize() const {
    return BASE_CHUNK_SIZE + WAVE_ID_SIZE;
}

const uint8_t *RiffChunk::getRiffType() const {
    return this->RiffType;
}

void RiffChunk::setRiffType(uint8_t *WAVEID){
    std::copy(WAVEID, WAVEID + WAVE_ID_SIZE, this->RiffType);
};

std::ostream &operator<<(std::ostream &stream, const RiffChunk &chunk) {
    return stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " RIFF " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl
    << ((BaseChunk &)chunk)
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| RIFF Type "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + string((const char *) chunk.getRiffType(), WAVE_ID_SIZE )+ " |" << endl
    << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;
}

RiffChunk::~RiffChunk() = default;