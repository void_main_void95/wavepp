#include <NoteChunk.h>
#include "NoteChunk.h"

#define DW_NAME_SIZE 4

NoteChunk::NoteChunk() {
    this->setCkID((uint8_t *)&"note");
}

NoteChunk::NoteChunk(const uint8_t *RaWData) : BaseAssociatedDataListMember(RaWData) {
    ;
}

std::ostream &operator<<(std::ostream &stream, const NoteChunk &chunk) {
    stream << ((BaseAssociatedDataListMember &)chunk);

    if (ceil((chunk.getCkSize() - DW_NAME_SIZE)/(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 10)) == 0) {
        stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Text "
               << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
               <<" " +  string((const char *)(chunk.getData().data()), chunk.getCkSize() - DW_NAME_SIZE) + " |  |" << endl;
    }

    else{
        stream << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Text --->"
               << setfill(' ') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " |  |" << endl;

        if (ceil((chunk.getCkSize() - DW_NAME_SIZE)/(MAX_PRINT_SIZE - 10)) == 0){
            stream << "|  | " + string((const char *) (chunk.getData().data()), (MAX_PRINT_SIZE - 10))
                   << " |  |" << endl;
        }
        else {
            for (uint32_t i = 0; i <= ceil((chunk.getCkSize() - DW_NAME_SIZE) / (MAX_PRINT_SIZE - 10)); i++) {
                stream << "|  | " + string((const char *) (chunk.getData().data() + i * (MAX_PRINT_SIZE - 10)),
                                           (MAX_PRINT_SIZE - 10))
                       << " |  |" << endl;
            }
        }
    }

    stream << "|  | " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << " |  |" << endl;

    return stream;
}

NoteChunk::~NoteChunk() = default;
