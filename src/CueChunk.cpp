#include <CueChunk.h>
#include "CueChunk.h"

#define NUM_CUE_POINTS_SIZE 4

CueChunk::CueChunk() {
    this->setCkID((uint8_t *)&"cue ");
    this->setCkSize(NUM_CUE_POINTS_SIZE);
    this->setPaddingRequired(false);
    this->NumCuePoints = 0;
    this->CuePoints = vector<CuePoint *>();
}

uint32_t CueChunk::getNumCuePoints() const {
    return this->NumCuePoints;
}

vector<CuePoint *> CueChunk::getCuePoints() const {
    return this->CuePoints;
}

CueChunk::CueChunk(const uint8_t * RawData) : BaseChunk(RawData){
    this->setPaddingRequired(false);
    std::copy(RawData + NUM_CUE_POINTS_OFFSET, RawData + CUE_POINTS_OFFSET, (uint8_t * )&NumCuePoints);
    auto *buffer = new uint8_t[CUE_POINT_SIZE]();
    this->NumCuePoints = (uint32_t )(this->getCkSize() - NUM_CUE_POINTS_SIZE)/CUE_POINT_SIZE;
    this->CuePoints = vector<CuePoint *>();
    this->CuePoints.reserve(this->NumCuePoints);
    for(uint32_t i=0; i < this->NumCuePoints; i++) {
        std::copy(RawData + CUE_POINTS_OFFSET + CUE_POINT_SIZE * i, RawData + CUE_POINTS_OFFSET + CUE_POINT_SIZE * (i + 1) , buffer);
        auto *tmp = new CuePoint(buffer);
        this->CuePoints.insert(this->CuePoints.end(), tmp);
        std::memset((void *)buffer,0, CUE_POINT_SIZE);
    }
    delete [] buffer;
}

int CueChunk::getRaw(vector<uint8_t> *raw){
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), (uint8_t *)&this->NumCuePoints, (uint8_t *)(&this->NumCuePoints + NUM_CUE_POINTS_SIZE));
    tmp = vector<uint8_t>(CUE_POINT_SIZE);
    for(uint32_t i=0; i < (this->getCkSize() - NUM_CUE_POINTS_SIZE)/CUE_POINT_SIZE; i++) {
        this->CuePoints.at(i)->getRaw(&tmp);
        raw->insert(raw->end(), tmp.begin(), tmp.end());
        tmp.clear();
    }
    tmp.clear();
    tmp.shrink_to_fit();
    return 0;
}

const uint32_t CueChunk::getRawSize() const{
    return BASE_CHUNK_SIZE + this->getCkSize() ;
}

void CueChunk::setNumCuePoints(uint32_t NumCuePoints) {
    CueChunk::NumCuePoints = NumCuePoints;
}

void CueChunk::setCuePoints(vector<CuePoint *> &CuePoints) {
    CueChunk::CuePoints = CuePoints;
}

std::ostream &operator<<(std::ostream &stream, const CueChunk &chunk) {
     stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " CUE " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl
                  << ((BaseChunk &)chunk)
                  << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Number of cue points "
                  << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << " " + to_string(chunk.getNumCuePoints()) + " |" << endl
                  << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "| Cue points --->"
                  << setfill(' ') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right << "|" << endl;

     stream << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
           << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;

     if (! chunk.getCuePoints().empty()){
        for(auto &i : chunk.getCuePoints()){
            stream << *(i)
            << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
            << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;
        }
     }
     stream << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;

    return stream;
}

CueChunk::~CueChunk() {
    for(auto &i : this->CuePoints){
        delete i;
    }
    this->CuePoints.clear();
    this->CuePoints.shrink_to_fit();
}
