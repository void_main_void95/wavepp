#include <InstrumentChunk.h>
#include "InstrumentChunk.h"

InstrumentChunk::InstrumentChunk() {
    this->setCkID((uint8_t *)&"inst");
    this->setCkSize(7);
    this->setPaddingRequired(true);
    this->bHighNote = 0;
    this->bHighVelocity = 0;
    this->bLowVelocity = 0;
    this->bLowNote = 0;
    this->chGain = 0;
    this->chFineTune = 0;
    this->bUnshiftedNote = 0;
    std::memset((void *)&this->bPaddingByte, 0, 1);
}

uint8_t InstrumentChunk::getBUnshiftedNote() const {
    return bUnshiftedNote;
}

void InstrumentChunk::setBUnshiftedNote(uint8_t bUnshiftedNote) {
    InstrumentChunk::bUnshiftedNote = bUnshiftedNote;
}

uint8_t InstrumentChunk::getChFineTune() const {
    return chFineTune;
}

void InstrumentChunk::setChFineTune(uint8_t chFineTune) {
    InstrumentChunk::chFineTune = chFineTune;
}

uint8_t InstrumentChunk::getChGain() const {
    return chGain;
}

void InstrumentChunk::setChGain(uint8_t chGain) {
    InstrumentChunk::chGain = chGain;
}

uint8_t InstrumentChunk::getBLowNote() const {
    return bLowNote;
}

void InstrumentChunk::setBLowNote(uint8_t bLowNote) {
    InstrumentChunk::bLowNote = bLowNote;
}

uint8_t InstrumentChunk::getBHighNote() const {
    return bHighNote;
}

void InstrumentChunk::setBHighNote(uint8_t bHighNote) {
    InstrumentChunk::bHighNote = bHighNote;
}

uint8_t InstrumentChunk::getBLowVelocity() const {
    return bLowVelocity;
}

void InstrumentChunk::setBLowVelocity(uint8_t bLowVelocity) {
    InstrumentChunk::bLowVelocity = bLowVelocity;
}

uint8_t InstrumentChunk::getBHighVelocity() const {
    return bHighVelocity;
}

void InstrumentChunk::setBHighVelocity(uint8_t bHighVelocity) {
    InstrumentChunk::bHighVelocity = bHighVelocity;
}

InstrumentChunk::InstrumentChunk(const uint8_t *RaWData) : BaseChunk(RaWData) {
    this->bHighNote = 0;
    this->bHighVelocity = 0;
    this->bLowVelocity = 0;
    this->bLowNote = 0;
    this->chGain = 0;
    this->chFineTune = 0;
    this->bUnshiftedNote = 0;
    std::copy(RaWData + B_UNSHIFTED_NOTE_OFFSET, RaWData + CH_FINE_TUNE_OFFSET, &this->bUnshiftedNote);
    std::copy(RaWData + CH_FINE_TUNE_OFFSET, RaWData + CH_GAIN_OFFSET, &this->chFineTune);
    std::copy(RaWData + CH_GAIN_OFFSET, RaWData + B_LOW_NOTE_OFFSET, &this->chGain);
    std::copy(RaWData + B_LOW_NOTE_OFFSET, RaWData + B_HIGH_NOTE_OFFSET, &this->bLowNote);
    std::copy(RaWData + B_HIGH_NOTE_OFFSET, RaWData + B_LOW_VELOCITY_OFFSET, &this->bHighNote);
    std::copy(RaWData + B_LOW_VELOCITY_OFFSET, RaWData + B_HIGH_VELOCITY_OFFSET, &this->bLowVelocity);
    std::copy(RaWData + B_HIGH_VELOCITY_OFFSET, RaWData + BASE_CHUNK_SIZE + this->getCkSize(), &this->bHighVelocity);
    std::memset((void *)&this->bPaddingByte, 0, 1);
}

int InstrumentChunk::getRaw(vector<uint8_t> *raw) {
    auto tmp = vector<uint8_t >();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getCkSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), &this->bUnshiftedNote,&this->bUnshiftedNote +1);
    raw->insert(raw->end(), &this->chFineTune,&this->chFineTune +1);
    raw->insert(raw->end(), &this->chGain,&this->chGain +1);
    raw->insert(raw->end(), &this->bLowNote,&this->bLowNote +1);
    raw->insert(raw->end(), &this->bHighNote,&this->bHighNote +1);
    raw->insert(raw->end(), &this->bLowVelocity,&this->bLowVelocity +1);
    raw->insert(raw->end(), &this->bHighVelocity,&this->bHighVelocity +1);
    raw->insert(raw->end(), &this->bPaddingByte, &this->bPaddingByte + 1);
    return 0;
}

const uint32_t InstrumentChunk::getRawSize() const{
    return BASE_CHUNK_SIZE + this->getCkSize() + 1;
}

std::ostream &operator<<(std::ostream &stream, const InstrumentChunk &chunk) {

    return stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " INST " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl
                                    << ((BaseChunk &)chunk)
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Unshifted note "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getBUnshiftedNote()) + " |" << endl
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Fine tune "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getChFineTune()) + " |" << endl
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Gain "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getChGain()) + " |" << endl
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Low note "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getBLowNote()) + " |" << endl
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| High note "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getBHighNote()) + " |" << endl
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Low velocity "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getBLowVelocity()) + " |" << endl
                                    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| High velocity "
                                    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) <<
                                    right << " " + to_string(chunk.getBHighVelocity()) + " |" << endl
                                    << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;
}


InstrumentChunk::~InstrumentChunk() = default;