#include <CuePoint.h>
#include "CuePoint.h"

#define DW_NAME_SIZE 4
#define DW_POSITION_SIZE 4
#define DW_FCC_CHUNK_SIZE 4
#define DW_CHUNK_START_SIZE 4
#define DW_BLOCK_START_SIZE 4
#define DW_SAMPLE_OFFSET_SIZE 4

CuePoint::CuePoint() {
    this->dwName = 0;
    this->dwPosition = 0;
    this->dwBlockStart = 0;
    this->dwSampleOffset = 0;
};

CuePoint::CuePoint(const uint8_t *RawData) {
    std::copy(RawData + DW_NAME_OFFSET, RawData + DW_POSITION_OFFSET, (uint8_t *)&this->dwName);
    std::copy(RawData + DW_POSITION_OFFSET, RawData + DW_FCC_CHUNK_OFFSET, (uint8_t *)&this->dwPosition);
    std::copy(RawData + DW_FCC_CHUNK_OFFSET, RawData + DW_CHUNK_START_OFFSET, this->fccChunk);
    std::copy(RawData + DW_CHUNK_START_OFFSET, RawData + DW_BLOCK_START_OFFSET, (uint8_t *)&this->dwChunkStart);
    std::copy(RawData + DW_BLOCK_START_OFFSET, RawData + DW_SAMPLE_OFFSET_OFFSET, (uint8_t *)&this->dwBlockStart);
    std::copy(RawData + DW_SAMPLE_OFFSET_OFFSET, RawData + CUE_POINT_SIZE, (uint8_t *)&this->dwSampleOffset);
}

int CuePoint::getRaw(vector<uint8_t> *raw){
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), (uint8_t *)&this->dwName, (uint8_t *)(&this->dwName + DW_NAME_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->dwPosition, (uint8_t *)(&this->dwPosition + DW_POSITION_SIZE));
    raw->insert(raw->end(), this->fccChunk, this->fccChunk + DW_FCC_CHUNK_SIZE);
    raw->insert(raw->end(), (uint8_t *)&this->dwChunkStart, (uint8_t *)(&this->dwChunkStart + DW_CHUNK_START_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->dwBlockStart, (uint8_t *)(&this->dwBlockStart + DW_BLOCK_START_SIZE));
    raw->insert(raw->end(), (uint8_t *)&this->dwSampleOffset, (uint8_t *)(&this->dwSampleOffset + DW_SAMPLE_OFFSET_SIZE));
    return 0;
}

uint32_t CuePoint::getDwSampleOffset() const {
    return this->dwSampleOffset;
}

uint32_t CuePoint::getDwName() const {
    return this->dwName;
}

uint32_t CuePoint::getDwPosition() const {
    return this->dwPosition;
}

const uint8_t *CuePoint::getFccChunk() const {
    return this->fccChunk;
}

uint32_t CuePoint::getDwChunkStart() const {
    return this->dwChunkStart;
}

uint32_t CuePoint::getDwBlockStart() const {
    return this->dwBlockStart;
}

const uint32_t CuePoint::getRawSize() const{
    return CUE_POINT_SIZE;
}

void CuePoint::setDwName(uint32_t dwName) {
    CuePoint::dwName = dwName;
}

void CuePoint::setDwPosition(uint32_t dwPosition) {
    CuePoint::dwPosition = dwPosition;
}

void CuePoint::setDwChunkStart(uint32_t dwChunkStart) {
    CuePoint::dwChunkStart = dwChunkStart;
}

void CuePoint::setDwBlockStart(uint32_t dwBlockStart) {
    CuePoint::dwBlockStart = dwBlockStart;
}

void CuePoint::setDwSampleOffset(uint32_t dwSampleOffset) {
    CuePoint::dwSampleOffset = dwSampleOffset;
}

void CuePoint::setFccChunk(const uint8_t *fccChunk) {
    std::copy(fccChunk, fccChunk + 4, this->fccChunk);
}

std::ostream &operator<<(std::ostream &stream, const CuePoint &chunk) {
    return stream << "|  | " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | ID "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwName()) + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Position "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwPosition()) + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Data chunk ID "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " +
    CuePoint::stripNonPrintable(string((const char*)chunk.getFccChunk(), DW_FCC_CHUNK_SIZE),'*') + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Chunk start "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwChunkStart()) + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Block start "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwBlockStart()) + " |  |" << endl
    << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Sample start "
    << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " " + to_string(chunk.getDwSampleOffset()) + " |  |" << endl
    << "|  | " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << " |  |" << endl;
}

CuePoint::~CuePoint() = default;

const string CuePoint::stripNonPrintable(const string &NonPrintableString, char filler='.') {
    auto printable = string();
    for(auto &i : NonPrintableString){
        if (not isprint(i)){
            printable += filler;
        }
        else{
            printable += i;
        }
    }
    return printable;
}