#include <DataChunk.h>
#include "DataChunk.h"
#include <iostream>

DataChunk::DataChunk() : BaseChunk(){
    this->setCkID((uint8_t *)&"data");
    this->setCkSize(0);
    this->ckData = std::vector<uint8_t>();
}

DataChunk::DataChunk(uint8_t * RawData) : BaseChunk(RawData){
    if (this->isPaddingRequired()){
        this->ckData.insert(this->ckData.end(), RawData + CK_DATA_OFFSET, RawData + CK_DATA_OFFSET + this->getCkSize() + 1);
    }
    else{
        this->ckData.insert(this->ckData.end(), RawData + CK_DATA_OFFSET, RawData + CK_DATA_OFFSET + this->getCkSize());
    }
}

int DataChunk::getRaw(std::vector<uint8_t> *raw){
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), this->ckData.begin(), this->ckData.end());
    return 0;
}

const uint32_t DataChunk::getRawSize() const{
    if (this->isPaddingRequired()){
        return BASE_CHUNK_SIZE + getCkSize() + 1;
    }
    return BASE_CHUNK_SIZE + getCkSize();
}

std::ostream &operator<<(std::ostream &stream, const DataChunk &chunk) {
     stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " DATA " << setw((MAX_PRINT_SIZE / 2)) << left << "CHUNK " << endl
                  << ((BaseChunk &)chunk)
                  << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Data "
                  << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " stored samples |" << endl
                  << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;
    return stream;
}

DataChunk::~DataChunk() {
    this->ckData.clear();
    this->ckData.shrink_to_fit();
}

const vector<uint8_t> &DataChunk::getCkData() const {
    return ckData;
}

void DataChunk::setCkData(const vector<uint8_t> &ckData) {
    this->ckData.clear();
    this->ckData.shrink_to_fit();
    this->setCkSize(ckData.size());
    if (this->isPaddingRequired()){
        this->ckData.reserve(ckData.size() + 1);
        this->ckData.insert(this->ckData.end(), ckData.begin(), ckData.end());
        this->ckData.insert(this->ckData.end(), 0);
    }
    else{
        this->ckData.reserve(ckData.size());
        this->ckData.insert(this->ckData.end(), ckData.begin(), ckData.end());
    }
}

