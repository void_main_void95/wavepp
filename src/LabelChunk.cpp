#include <LabelChunk.h>
#include "LabelChunk.h"

#define DW_NAME_SIZE 4

LabelChunk::LabelChunk() {
    this->setCkID((uint8_t *)&"labl");
}

LabelChunk::LabelChunk(const uint8_t *RaWData) : BaseAssociatedDataListMember(RaWData) {
    ;
}

std::ostream &operator<<(std::ostream &stream, const LabelChunk &chunk) {
    stream << ((BaseAssociatedDataListMember &)chunk);

    if (ceil((chunk.getCkSize() - DW_NAME_SIZE)/(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE - 10)) == 0) {
        stream << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Text "
               << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right
               <<" " +  BaseChunk::stripNonPrintable(string((const char *)(chunk.getData().data()), chunk.getCkSize() - DW_NAME_SIZE), '*' ) + " |  |" << endl;
    }

    else{
        stream << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|  | Text --->"
               << setfill(' ') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << " |  |" << endl;

        if (ceil((chunk.getCkSize() - DW_NAME_SIZE)/(MAX_PRINT_SIZE - 10)) == 0){
            stream << "|  | " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getData().data()), (MAX_PRINT_SIZE - 10)), '*')
                   << " |  |" << endl;
        }
        else {
            for (uint32_t i = 0; i <= ceil((chunk.getCkSize() - DW_NAME_SIZE) / (MAX_PRINT_SIZE - 10)); i++) {
                stream << "|  | " + BaseChunk::stripNonPrintable(string((const char *) (chunk.getData().data() + i * (MAX_PRINT_SIZE - 10)),
                                           (MAX_PRINT_SIZE - 10)), '*')
                       << " |  |" << endl;
            }
        }
    }

    stream << "|  | " << setfill('-') << setw(MAX_PRINT_SIZE - 10) << '-' << " |  |" << endl;

    return stream;
}

LabelChunk::~LabelChunk() = default;
