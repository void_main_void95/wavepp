#include <AssociatedDataListChunk.h>
#include "AssociatedDataListChunk.h"

#define TYPE_ID_SIZE 4

const uint8_t *AssociatedDataListChunk::getTypeID() const {
    return TypeID;
}

void AssociatedDataListChunk::setTypeID(const uint8_t *TypeID) {
    std::copy(TypeID, TypeID + TYPE_ID_SIZE, this->TypeID);

}

AssociatedDataListChunk::AssociatedDataListChunk() {
    this->setCkID((uint8_t *)&"list");
    this->setCkSize(TYPE_ID_SIZE);
    this->setTypeID((uint8_t *)&"adtl");
    this->LabelChunkList = vector<LabelChunk *>();
    this->LabeledTextChunkList = vector<LabeledTextChunk *>();
    this->NoteChunkList = vector<NoteChunk *>();
}

AssociatedDataListChunk::AssociatedDataListChunk(const uint8_t *RaWData) : BaseChunk(RaWData) {
    this->LabelChunkList = vector<LabelChunk *>();
    this->LabeledTextChunkList = vector<LabeledTextChunk *>();
    this->NoteChunkList = vector<NoteChunk *>();
    std::copy(RaWData + TYPE_ID_OFFSET, RaWData + ADTL_OFFSET, this->TypeID);
    uint8_t *header_buffer = nullptr;
    header_buffer = new uint8_t[BASE_CHUNK_SIZE]();
    uint32_t already_read = 0;
    uint32_t tmp_chunk_size = 0;
    while (already_read < (this->getCkSize() - TYPE_ID_SIZE)) {
        std::copy(RaWData + ADTL_OFFSET + already_read, RaWData + ADTL_OFFSET + already_read + BASE_CHUNK_SIZE, header_buffer);
        std::copy(header_buffer + CK_SIZE_OFFSET, header_buffer + BASE_CHUNK_SIZE, (uint8_t *) &tmp_chunk_size);
        if (tmp_chunk_size % 2 != 0){
            tmp_chunk_size += 1;
        }
        auto *buffer = new uint8_t[tmp_chunk_size + BASE_CHUNK_SIZE]();

        std::copy(header_buffer, header_buffer + BASE_CHUNK_SIZE, buffer);
        std::copy(RaWData + ADTL_OFFSET + already_read + BASE_CHUNK_SIZE,
                  RaWData + ADTL_OFFSET + already_read + BASE_CHUNK_SIZE + tmp_chunk_size, buffer + BASE_DW_NAME_OFFSET);
        if (std::strncmp((const char *) header_buffer, "labl", 4) == 0) {
            auto *tmp = new LabelChunk(buffer);
            this->LabelChunkList.push_back(tmp);
            already_read += tmp_chunk_size + BASE_CHUNK_SIZE;
        } else if (std::strncmp((const char *) header_buffer, "note", 4) == 0) {
            auto *tmp = new NoteChunk(buffer);
            this->NoteChunkList.push_back(tmp);
            already_read += tmp_chunk_size + BASE_CHUNK_SIZE;
        } else if (std::strncmp((const char *) header_buffer, "ltxt", 4) == 0) {
            auto *tmp = new LabeledTextChunk(buffer);
            this->LabeledTextChunkList.push_back(tmp);
            already_read += tmp_chunk_size + BASE_CHUNK_SIZE;
        }
        std::memset((void *)header_buffer, 0, BASE_CHUNK_SIZE);
        tmp_chunk_size = 0;
        delete [] buffer;
    }
    delete [] header_buffer;
}

int AssociatedDataListChunk::getRaw(vector<uint8_t> *raw) {
    auto tmp = vector<uint8_t>();
    BaseChunk::getRaw(&tmp);
    raw->reserve(this->getRawSize());
    raw->insert(raw->end(), tmp.begin(), tmp.end());
    tmp.clear();
    tmp.shrink_to_fit();
    raw->insert(raw->end(), this->TypeID, this->TypeID + TYPE_ID_SIZE);
    if (not this->LabelChunkList.empty()){
        for (auto &i : this->LabelChunkList) {
            tmp = vector<uint8_t>(i->getRawSize());
            i->getRaw(&tmp);
            raw->insert(raw->end(), tmp.begin(), tmp.end());
            tmp.clear();
        }
        tmp.shrink_to_fit();
    }
    if (not this->NoteChunkList.empty()){
        for (auto &i :this->NoteChunkList) {
            tmp = vector<uint8_t>(i->getRawSize());
            i->getRaw(&tmp);
            raw->insert(raw->end(), tmp.begin(), tmp.end());
            tmp.clear();
        }
        tmp.shrink_to_fit();
    }
    if (not this->LabeledTextChunkList.empty()){
        for (auto &i : this->LabeledTextChunkList) {
            tmp = vector<uint8_t>(i->getRawSize());
            i->getRaw(&tmp);
            raw->insert(raw->end(), tmp.begin(), tmp.end());
            tmp.clear();
        }
        tmp.shrink_to_fit();
    }
    return 0;
}

const uint32_t AssociatedDataListChunk::getRawSize() const{
    return BASE_CHUNK_SIZE + this->getCkSize();
}

vector<LabelChunk *> AssociatedDataListChunk::getLabelChunkList() const {
    return LabelChunkList;
}

void AssociatedDataListChunk::setLabelChunkList(vector<LabelChunk *> &LabelChunkList) {
    AssociatedDataListChunk::LabelChunkList = LabelChunkList;
}

vector<NoteChunk *> AssociatedDataListChunk::getNoteChunkList() const {
    return NoteChunkList;
}

void AssociatedDataListChunk::setNoteChunkList(vector<NoteChunk *> &NoteChunkList) {
    AssociatedDataListChunk::NoteChunkList = NoteChunkList;
}

vector<LabeledTextChunk *> AssociatedDataListChunk::getLabeledTextChunkList() const {
    return LabeledTextChunkList;
}

void AssociatedDataListChunk::setLabeledTextChunkList(vector<LabeledTextChunk *> &LabeledTextChunkList) {
    AssociatedDataListChunk::LabeledTextChunkList = LabeledTextChunkList;
}

std::ostream &operator<<(std::ostream &stream, const AssociatedDataListChunk &chunk) {
   stream << setfill('#') << setw(MAX_PRINT_SIZE / 2) << right << " ASSOCIATED DATA " << setw((MAX_PRINT_SIZE / 2)) << left << "LIST CHUNK " << endl
                  << ((BaseChunk &)chunk)
                  << setfill('.') << setw(MAX_KEY_PRINT_SIZE) << left << "| Type ID "
                  << setfill('.') << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE ) << right
                  << " " + string((const char *)chunk.getTypeID(), TYPE_ID_SIZE) + " |" << endl;

   stream << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
           << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;

   if (not chunk.getLabelChunkList().empty()){
       for(auto &i : chunk.getLabelChunkList()){
           stream << *(i)
                  << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
                  << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;
       }
   }

    if (not chunk.getNoteChunkList().empty()){
        for(auto &i : chunk.getNoteChunkList()){
            stream << *(i)
                   << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
                   << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;
        }
    }

    if (not chunk.getLabeledTextChunkList().empty()){
        for(auto &i : chunk.getLabeledTextChunkList()){
            stream << *(i)
                   << setfill(' ') << setw(MAX_KEY_PRINT_SIZE) << left << "|" << setfill(' ')
                   << setw(MAX_PRINT_SIZE - MAX_KEY_PRINT_SIZE) << right << "|" << endl;
        }
    }

    stream << setfill('#') << setw(MAX_PRINT_SIZE) << '#' << endl;

    return stream;
}

AssociatedDataListChunk::~AssociatedDataListChunk() {
    if (not this->NoteChunkList.empty()){
        for (auto &i : this->NoteChunkList) {
            delete i;
        }
        this->NoteChunkList.clear();
        this->NoteChunkList.shrink_to_fit();
    }
    if (not this->LabelChunkList.empty()){
        for (auto &i : this->LabelChunkList) {
            delete i;
        }
        this->LabelChunkList.clear();
        this->LabelChunkList.shrink_to_fit();
    }
    if (not this->LabeledTextChunkList.empty()){
        for (auto &i : this->LabeledTextChunkList) {
            delete i;
        }
        this->LabeledTextChunkList.clear();
        this->LabeledTextChunkList.shrink_to_fit();
    }
}