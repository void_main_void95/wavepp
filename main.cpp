#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <array>
#include <climits>
#include <sstream>
#include <unistd.h>
#include <PcmWaveFile.h>
#include <wFormatTags.h>
#include <getopt.h>

using std::cin;
using std::cout;
using std::endl;
using std::fstream;
using std::string;

int main(int argc, char* argv[])
{
    static struct option params[] = {
            {"describe", no_argument, nullptr, 'a'},
            {"merge", no_argument, nullptr, 'm'},
            {"split", no_argument, nullptr, 's'},
            {"help", no_argument, nullptr, 'h'},
            {nullptr, 0, nullptr, 0}
    };
    string fileName;
    bool d_flag = false;
    bool m_flag = false;
    bool s_flag = false;
    bool h_flag = false;
    PcmWaveFile * file;
    int c = 0;
    while ((c = getopt_long(argc, argv, "dmsh", params, nullptr)) != -1){
        switch(c){
            case 'd':
                d_flag = true;
                break;
            case 'm':
                m_flag = true;
                break;
            case 's':
                s_flag = true;
                break;
            case 'h':
                h_flag = true;
                break;
            default:
                break;
        }
    }
    if (optind < argc)
    {
        fileName = string(argv[optind]);
    }
    if (m_flag and s_flag){
        cout << "Error: you can't use -s and -m simultaneously" << endl;
        return 1;
    }
    if (h_flag or argc == 1){
        cout << "Use: example-use [OPTIONS] FILE" << endl;
        cout << "Default:" << endl;
        cout << "\t Print file's summary to stdout" << endl;
        cout << "Options: " << endl;
        cout << "\t-d, --describe             Write a summary file which describe file's structure." << endl;
        cout << "\t-m, --merge                Convert stereo PCM into mono PCM using both channels." << endl;
        cout << "\t-s, --split                Split stereo PCM in two files, one per channel." << endl;
        cout << "\t-h, --help                 Print this help." << endl;
        cout << endl;
        return 0;
    }
    try{
        file = new PcmWaveFile(fileName);
    }
    catch( const std::ifstream::failure& e ) {
        cout << "Error: file not found!" << endl;
        return 1;
    }
    if (not m_flag and not s_flag and not d_flag and not h_flag){
        cout << *(file);
        delete file;
        return 0;
    }
    if (d_flag and not m_flag and not s_flag and not h_flag){
        file->WriteToFile();
        delete file;
        return 0;
    }
    if (file->getFmt()->getNChannels() < 2 or file->getFmt()->getWFormatTag() != WAVE_FORMAT_PCM){
        cout << "Error: Split and merge are possible only with stereo PCM file. " << endl;
        cout << "Provided file:" << endl;
        cout << "\tFormat tag: " << file->getFmt()->getWFormatTag() << endl;
        cout << "\tNumber of channel: " << file->getFmt()->getNChannels() << endl;
        return 1;
    }
    if(m_flag){
        PcmWaveFile * outfile;
        outfile = new PcmWaveFile();
        auto *out_riff_chunk = new RiffChunk();
        auto *out_fmt_chunk = new FmtChunk();
        out_fmt_chunk->setCkSize(16);
        out_fmt_chunk->setWFormatTag(WAVE_FORMAT_PCM);
        out_fmt_chunk->setNChannels(1);
        out_fmt_chunk->setNSamplesPerSec(file->getFmt()->getNSamplesPerSec());
        out_fmt_chunk->setWBitsPerSample(file->getFmt()->getWBitsPerSample());
        auto merged = vector<uint8_t >();
        if (file->pcmStereoToMono(&merged, 0.75, 0.25)){
            cout << "Error: Split and merge are possible only with stereo PCM (8 or 16 bit) files. " << endl;
            cout << "Provided file:" << endl;
            cout << "\tFormat tag: " << file->getFmt()->getWFormatTag() << endl;
            cout << "\tNumber of channel: " << file->getFmt()->getNChannels() << endl;
            cout << "\tBits per sample: " << file->getFmt()->getWBitsPerSample();
            delete file;
            delete out_fmt_chunk;
            delete out_riff_chunk;
            delete outfile;
            merged.clear();
            merged.shrink_to_fit();
            return 1;
        }
        auto *out_data_chunk = new DataChunk();
        out_data_chunk->setCkData(merged);
        merged.clear();
        merged.shrink_to_fit();
        outfile->setRIFF(out_riff_chunk);
        outfile->setFmt(out_fmt_chunk);
        outfile->setData(out_data_chunk);
        outfile->checkAndRestore();
        outfile->setFileName(file->getFileFullPath() + "-merged");
        outfile->WriteToFile();
        if (d_flag){
            outfile->writeInfo();
        }
        delete outfile;
    }
    if(s_flag){
        auto * outfile = new PcmWaveFile();
        auto *out_riff_chunk = new RiffChunk();
        auto *out_fmt_chunk = new FmtChunk();
        out_fmt_chunk->setCkSize(16);
        out_fmt_chunk->setWFormatTag(WAVE_FORMAT_PCM);
        out_fmt_chunk->setNChannels(1);
        out_fmt_chunk->setNSamplesPerSec(file->getFmt()->getNSamplesPerSec());
        out_fmt_chunk->setWBitsPerSample(file->getFmt()->getWBitsPerSample());
        auto temp = std::vector<uint8_t >();
        temp.reserve(file->getPCMSingleChannelSize());
        if (file->getPCMLeftChannel(&temp)){
            cout << "Error: Split and merge are possible only with stereo PCM file. " << endl;
            cout << "Provided file:" << endl;
            cout << "\tFormat tag: " << file->getFmt()->getWFormatTag() << endl;
            cout << "\tNumber of channel: " << file->getFmt()->getNChannels() << endl;
            delete file;
            delete out_fmt_chunk;
            delete out_riff_chunk;
            delete outfile;
            temp.clear();
            temp.shrink_to_fit();
            return 1;
        }
        auto *out_data_chunk_left = new DataChunk;
        out_data_chunk_left->setCkData(temp);
        outfile->setRIFF(out_riff_chunk);
        outfile->setFmt(out_fmt_chunk);
        outfile->setData(out_data_chunk_left);
        temp.clear();
        temp.shrink_to_fit();
        outfile->checkAndRestore();
        outfile->setFileName(file->getFileFullPath() + "-left");
        if (d_flag){
            outfile->writeInfo();
        }
        outfile->WriteToFile();
        file->getPCMRightChannel(&temp);
        outfile->getData()->setCkData(temp);
        temp.clear();
        temp.shrink_to_fit();
        outfile->checkAndRestore();
        outfile->setFileName(file->getFileFullPath() + "-right");
        outfile->WriteToFile();
        if (d_flag){
            outfile->writeInfo();
        }
        delete outfile;
    }
    delete file;

    return 0;
}
