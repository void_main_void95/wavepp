#ifndef WAVEPP_PLAYLISTCHUNK_H
#define WAVEPP_PLAYLISTCHUNK_H

#include <vector>
#include <BaseChunk.h>
#include <PlaylistSegment.h>

/*this chunk is always word aligned
 CkSize= 4 + (NumOfPlaySegment * 12)
 This object can be read and write without padding byte
 */

#define DW_SEGMENTS_OFFSET 8
#define PLAY_SEGMENT_OFFSET 12

using namespace std;

class PlaylistChunk : public BaseChunk{

public:
    PlaylistChunk();
    explicit PlaylistChunk(const uint8_t *RawData);
    ~PlaylistChunk() override;
    uint32_t getDwSegments() const;
    void setDwSegments(uint32_t dwSegments);
    vector<PlaylistSegment *> getPlaySegment() const;
    void setPlaySegment(vector<PlaylistSegment *> &play_segment);
    int getRaw(std::vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const PlaylistChunk&chunk);

private:
    uint32_t dwSegments;
    std::vector<PlaylistSegment *> PlaySegment;
};


#endif //WAVEPP_PLAYLISTCHUNK_H
