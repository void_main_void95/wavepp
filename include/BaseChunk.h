#ifndef WAVEPP_BASECHUNK_H
#define WAVEPP_BASECHUNK_H

#include <cstdint>
#include <algorithm>
#include <ostream>
#include <iomanip>
#include <cstring>
#include <misc.h>
#include <memory>
#include <vector>
#include <iostream>
#include <iterator>
#include <cmath>
#include <cctype>

#define BASE_CHUNK_SIZE 8
#define CK_ID_OFFSET 0
#define CK_SIZE_OFFSET 4

using namespace std;

class BaseChunk {

public:
    BaseChunk();
    explicit BaseChunk(const uint8_t * RaWData);
    virtual ~BaseChunk();
    const uint8_t *getCkID() const;
    uint32_t getCkSize() const;
    void setCkSize(uint32_t ckSize);
    void setCkID(uint8_t * ckID);
    bool isPaddingRequired() const;
    void setPaddingRequired(bool paddingRequired);
    friend std::ostream& operator<<(std::ostream&stream, const BaseChunk&chunk);
    virtual int getRaw(std::vector<uint8_t> *raw);
    virtual const uint32_t getRawSize() const;
    static const string  stripNonPrintable(const string &NonPrintableString, char filler);

private:
    uint8_t ckID[4]{};
    uint32_t ckSize{};
    bool paddingRequired;
};


#endif //MP3ENCODER_BASECHUNK_H
