#ifndef WAVEPP_ASSOCIATEDDATALISTCHUNK_H
#define WAVEPP_ASSOCIATEDDATALISTCHUNK_H

/*this chunk is always word aligned.
 Adtl list member already handle padding byte.
 This object can be read and write without padding byte.
 */

#include <cstdint>
#include <cstring>
#include <vector>
#include <BaseChunk.h>
#include <LabelChunk.h>
#include <NoteChunk.h>
#include <LabeledTextChunk.h>

#define TYPE_ID_OFFSET 8
#define ADTL_OFFSET 12

using namespace std;

class AssociatedDataListChunk : public BaseChunk{

public:
    AssociatedDataListChunk();
    explicit AssociatedDataListChunk(const uint8_t *RaWData);
    ~AssociatedDataListChunk() override;
    const uint8_t *getTypeID() const;
    void setTypeID(const uint8_t *TypeID);
    vector<LabelChunk *> getLabelChunkList() const;
    void setLabelChunkList(vector<LabelChunk *> &LabelChunkList);
    vector<NoteChunk *> getNoteChunkList() const;
    void setNoteChunkList(vector<NoteChunk *> &NoteChunkList);
    vector<LabeledTextChunk *> getLabeledTextChunkList() const;
    void setLabeledTextChunkList(vector<LabeledTextChunk *> &LabeledTextChunkList);
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const AssociatedDataListChunk&chunk);

private:
    uint8_t TypeID[4]{};
    std::vector<LabelChunk *> LabelChunkList;
    std::vector<NoteChunk *> NoteChunkList;
    std::vector<LabeledTextChunk *> LabeledTextChunkList;

};


#endif //WAVEPP_ASSOCIATEDDATALISTCHUNK_H
