#ifndef WAVEPP_INSTRUMENTCHUNK_H
#define WAVEPP_INSTRUMENTCHUNK_H

/*this chunk isn't word aligned
 CkSize= 7
 This object need padding byte for read and write
 */

#include <BaseChunk.h>

#define B_UNSHIFTED_NOTE_OFFSET 8
#define CH_FINE_TUNE_OFFSET 9
#define CH_GAIN_OFFSET 10
#define B_LOW_NOTE_OFFSET 11
#define B_HIGH_NOTE_OFFSET 12
#define B_LOW_VELOCITY_OFFSET 13
#define B_HIGH_VELOCITY_OFFSET 14

using namespace std;

class InstrumentChunk : public BaseChunk{

public:
    InstrumentChunk();
    explicit InstrumentChunk(const uint8_t *RaWData);
    ~InstrumentChunk() override;
    uint8_t getBUnshiftedNote() const;
    void setBUnshiftedNote(uint8_t bUnshiftedNote);
    uint8_t getChFineTune() const;
    void setChFineTune(uint8_t chFineTune);
    uint8_t getChGain() const;
    void setChGain(uint8_t chGain);
    uint8_t getBLowNote() const;
    void setBLowNote(uint8_t bLowNote);
    uint8_t getBHighNote() const;
    void setBHighNote(uint8_t bHighNote);
    uint8_t getBLowVelocity() const;
    void setBLowVelocity(uint8_t bLowVelocity);
    uint8_t getBHighVelocity() const;
    void setBHighVelocity(uint8_t bHighVelocity);
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const InstrumentChunk&chunk);

private:
    uint8_t bUnshiftedNote{};
    uint8_t chFineTune{};
    uint8_t chGain{};
    uint8_t bLowNote{};
    uint8_t bHighNote{};
    uint8_t bLowVelocity{};
    uint8_t bHighVelocity{};
    uint8_t bPaddingByte{};
};


#endif //WAVEPP_INSTRUMENTCHUNK_H
