#ifndef WAVEPP_PLAYLISTSEGMENT_H
#define WAVEPP_PLAYLISTSEGMENT_H

/*this chunk is always word aligned
 CkSize= 12
 This object can be read and write without padding byte
 */

#include <cstdint>
#include <vector>
#include <ostream>
#include <iomanip>
#include <misc.h>

#define PLAYLIST_SEGMENT_SIZE 12
#define DW_NAME_OFFSET 0
#define DW_LENGTH_OFFSET 4
#define DW_LOOPS_OFFSET 8

using namespace std;

class PlaylistSegment {

public:
    PlaylistSegment();
    explicit PlaylistSegment(const uint8_t *RawData);
    ~PlaylistSegment();
    uint32_t getDwName() const;
    uint32_t getDwLength() const;
    uint32_t getDwLoops() const;
    void setDwName(uint32_t dwName);
    void setDwLength(uint32_t dwLength);
    void setDwLoops(uint32_t dwLoops);
    int getRaw(vector<uint8_t> *raw);
    const uint32_t getRawSize() const;
    friend std::ostream& operator<<(std::ostream&stream, const PlaylistSegment&chunk);

private:
    uint32_t dwName{};
    uint32_t dwLength{};
    uint32_t dwLoops{};
};


#endif //WAVEPP_PLAYLISTSEGMENT_H
