#ifndef WAVEPP_CUECHUNK_H
#define WAVEPP_CUECHUNK_H

#include <vector>
#include <BaseChunk.h>
#include <CuePoint.h>

/*this chunk is always word aligned
 CkSize= 4 + (NumCuePoints * 24)
 This object can be read and write without padding byte
 */

#define NUM_CUE_POINTS_OFFSET 8
#define CUE_POINTS_OFFSET 12

using namespace std;

class CueChunk : public BaseChunk{

public:
    CueChunk();
    explicit CueChunk(const uint8_t * RawData);
    ~CueChunk() override;
    uint32_t getNumCuePoints() const;
    vector<CuePoint *> getCuePoints() const;
    void setNumCuePoints(uint32_t NumCuePoints);
    void setCuePoints(vector<CuePoint *> &CuePoints);
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const CueChunk&chunk);

private:
    uint32_t NumCuePoints;
    vector<CuePoint *> CuePoints;

};

#endif //MP3ENCODER_CUECHUNK_H
