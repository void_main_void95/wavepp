#ifndef WAVEPP_NOTECHUNK_H
#define WAVEPP_NOTECHUNK_H

#include <BaseAssociatedDataListMember.h>

class NoteChunk : public BaseAssociatedDataListMember{

public:
    NoteChunk();
    ~NoteChunk() override;
    explicit NoteChunk(const uint8_t *RaWData);
    friend std::ostream& operator<<(std::ostream&stream, const NoteChunk&chunk);

};


#endif //WAVEPP_NOTECHUNK_H
