#ifndef WAVEPP_LABELCHUNK_H
#define WAVEPP_LABELCHUNK_H

#include <BaseAssociatedDataListMember.h>

class LabelChunk : public BaseAssociatedDataListMember{

public:
    LabelChunk();
    ~LabelChunk() override;
    explicit LabelChunk(const uint8_t *RaWData);
    friend std::ostream& operator<<(std::ostream&stream, const LabelChunk&chunk);

};


#endif //WAVEPP_LABELCHUNK_H
