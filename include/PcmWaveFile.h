#ifndef WAVEPP_PCMWAVEFILE_H
#define WAVEPP_PCMWAVEFILE_H

#include <BaseWaveFile.h>
#include <memory>

class PcmWaveFile : public BaseWaveFile{

public:
    PcmWaveFile();
    explicit PcmWaveFile(const std::string& wav_file_path);
    ~PcmWaveFile() override;
    uint32_t getPCMSingleChannelSize();
    int pcmStereoToMono(std::vector<uint8_t> *out_channel, float left_channel_weight = 0.5, float right_channel_weight = 0.5);
    static int MergeChannels(std::vector<uint8_t> *out_channel, std::vector<uint8_t >* channel_1,
                      std::vector<uint8_t >* channel_2,
                uint16_t channel_1_WBitsPerSample,
                uint16_t channel_2_WBitsPerSample,
                uint16_t merged_WBitsPerSample,
                uint32_t merged_size,
                float channel_1_weight = 0.5, float channel_2_weight = 0.5);
    int getPCMRightChannel(std::vector<uint8_t> *out_channel);
    int getPCMLeftChannel(std::vector<uint8_t> *out_channel);
    bool checkFile() override;
    void checkAndRestore();

private:
    bool checkFmtIntegrity() override;
};

#endif