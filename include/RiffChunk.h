#ifndef WAVEPP_RIFFCHUNK_H
#define WAVEPP_RIFFCHUNK_H

#include <BaseChunk.h>

#define WAVE_ID_OFFSET 8

using namespace std;

class RiffChunk :public BaseChunk{

public:
    RiffChunk();
    explicit RiffChunk(const uint8_t *RaWData);
    ~RiffChunk() override;
    const uint8_t *getRiffType() const;
    void setRiffType(uint8_t *WAVEID);
    int getRaw(std::vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const RiffChunk&chunk);

private:
    uint8_t RiffType[4]{};
};


#endif //MP3ENCODER_RIFFCHUNK_H
