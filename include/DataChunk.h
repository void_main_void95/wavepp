#ifndef WAVEPP_DATACHUNK_H
#define WAVEPP_DATACHUNK_H

#include <BaseChunk.h>
#include <vector>


#define CK_DATA_OFFSET 8

using namespace std;

class DataChunk :public BaseChunk{

public:
    DataChunk();
    explicit DataChunk(uint8_t * RawData);
    ~DataChunk() override;
    const vector<uint8_t> &getCkData() const;
    void setCkData(const vector<uint8_t> &ckData);
    int getRaw(std::vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const DataChunk&chunk);

private:
    std::vector<uint8_t> ckData;
};


#endif //MP3ENCODER_DATACHUNK_H
