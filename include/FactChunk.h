#ifndef WAVEPP_FACTCHUNK_H
#define WAVEPP_FACTCHUNK_H

#include <BaseChunk.h>
#include <vector>

#define DW_SAMPLE_LENGHT_OFFSET 8
#define FACT_EXTRA_BYTES_OFFSET 12


class FactChunk : public  BaseChunk{

public:
    FactChunk();
    explicit FactChunk(const uint8_t *RawData);
    ~FactChunk() override;
    uint32_t getDwSampleLength() const;
    void setDwSampleLength(uint32_t dwSampleLength);
    vector<uint8_t> getExtraBytes() const;
    void setExtraBytes(vector<uint8_t> &ExtraBytes);
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const FactChunk&chunk);

private:
    uint32_t  dwSampleLength;
    std::vector<uint8_t> ExtraBytes;
};


#endif //WAVEPP_FACTCHUNK_H
