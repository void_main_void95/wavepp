#ifndef WAVEPP_CUEPOINT_H
#define WAVEPP_CUEPOINT_H


#include <cstdint>
#include <vector>
#include <iomanip>
#include <ostream>
#include <misc.h>

/*this chunk is always word aligned
 CkSize= 24
 This object can be read and write without padding byte
 */

#define CUE_POINT_SIZE 24
#define DW_NAME_OFFSET 0
#define DW_POSITION_OFFSET 4
#define DW_FCC_CHUNK_OFFSET 8
#define DW_CHUNK_START_OFFSET 12
#define DW_BLOCK_START_OFFSET 16
#define DW_SAMPLE_OFFSET_OFFSET 20

using namespace std;

class CuePoint{

public:
    CuePoint();
    explicit CuePoint(const uint8_t *RawData);
    ~CuePoint();
    uint32_t getDwSampleOffset() const;
    uint32_t getDwName() const;
    uint32_t getDwPosition() const;
    const uint8_t *getFccChunk() const;
    uint32_t getDwChunkStart() const;
    uint32_t getDwBlockStart() const;
    void setDwName(uint32_t dwName);
    void setDwPosition(uint32_t dwPosition);
    void setDwChunkStart(uint32_t dwChunkStart);
    void setDwBlockStart(uint32_t dwBlockStart);
    void setDwSampleOffset(uint32_t dwSampleOffset);
    void setFccChunk(const uint8_t *fccChunk);
    int getRaw(vector<uint8_t> *raw);
    const uint32_t getRawSize() const;
    friend std::ostream& operator<<(std::ostream&stream, const CuePoint&chunk);
    static const string  stripNonPrintable(const string &NonPrintableString, char filler);

private:
    uint32_t dwName{};
    uint32_t dwPosition{};
    uint8_t fccChunk[4]{};
    uint32_t dwChunkStart{};
    uint32_t dwBlockStart{};
    uint32_t dwSampleOffset{};
};


#endif //MP3ENCODER_CUEPOINT_H
