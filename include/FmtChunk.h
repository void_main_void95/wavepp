#ifndef WAVEPP_FMTCHUNK_H
#define WAVEPP_FMTCHUNK_H

#include <vector>
#include <BaseChunk.h>
#include <wFormatTags.h>

#define W_FORMAT_TAG_OFFSET 8
#define N_CHANNELS_OFFSET 10
#define N_SAMPLES_PER_SEC_OFFSET 12
#define N_AVG_BYTES_PER_SEC_OFFSET 16
#define N_BLOCK_ALIGN_OFFSET 20
#define W_BITS_PER_SAMPLES_OFFSET 22
#define CB_SIZE_OFFSET 24
#define FMT_EXTRA_BYTES_OFFSET 26

using namespace std;

class FmtChunk : public BaseChunk{

public:
    FmtChunk();
    explicit FmtChunk(const uint8_t *RaWData);
    ~FmtChunk() override;
    uint16_t getWFormatTag() const;
    uint16_t getNChannels() const;
    uint32_t getNSamplesPerSec() const;
    uint32_t getNAvgBytesPerSec() const;
    uint16_t getNBlockAlign() const;
    uint16_t getWBitsPerSample() const;
    void setWFormatTag(uint16_t wFormatTag);
    void setNChannels(uint16_t nChannels);
    void setNSamplesPerSec(uint32_t nSamplesPerSec);
    void setNAvgBytesPerSec(uint32_t nAvgBytesPerSec);
    void setNBlockAlign(uint16_t nBlockAlign);
    void setWBitsPerSample(uint16_t wBitsPerSample);
    void setCbSize(uint16_t cbSize);
    void setExtraBytes(vector<uint8_t> &ExtraBytes);
    uint16_t getCbSize() const;
    vector<uint8_t> getExtraBytes() const;
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const FmtChunk&chunk);

private:
    uint16_t wFormatTag;
    uint16_t nChannels;
    uint32_t nSamplesPerSec;
    uint32_t nAvgBytesPerSec{};
    uint16_t nBlockAlign;
    uint16_t wBitsPerSample;
    uint16_t cbSize;
    vector<uint8_t> ExtraBytes;
};


#endif //MP3ENCODER_FMTCHUNK_H
