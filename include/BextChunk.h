#ifndef WAVEPP_BEXTCHUNK_H
#define WAVEPP_BEXTCHUNK_H

#include <BaseChunk.h>
#include "BaseChunk.h"
#include <vector>


#define DESCRIPTION_OFFSET 8
#define ORIGINATOR_OFFSET 264
#define ORIGINATOR_REFERENCE_OFFSET 296
#define ORIGINATION_DATE_OFFSET 328
#define ORIGINATION_TIME_OFFSET 338
#define TIME_REFERENCE_LOW_OFFSET 346
#define TIME_REFERENCE_HIGH_OFFSET 350
#define VERSION_OFFSET 354
#define UMID_OFFSET 356
#define LOUDNESS_VALUE_OFFSET 420
#define LOUDNESS_RANGE_OFFSET 422
#define MAX_TRUE_PEAK_LEVEL_OFFSET 424
#define MAX_MOMENTARY_LOUDNESS_OFFSET 426
#define MAX_SHORT_TERM_LOUDNESS_OFFSET 428
#define RESERVED_OFFSET 430
#define CODING_HISTORY_OFFSET 610

using namespace std;

class BextChunk : public BaseChunk {

public:
    BextChunk();
    explicit BextChunk(const uint8_t *RawData);
    ~BextChunk() override;
    const uint8_t *getDescription() const;
    const uint8_t *getOriginator() const;
    const uint8_t *getOriginatorReference() const;
    const uint8_t *getOriginationDate() const;
    const uint8_t *getOriginationTime() const;
    uint32_t getTimeReferenceLow() const;
    uint32_t getTimeReferenceHigh() const;
    uint16_t getVersion() const;
    const uint8_t *getUmid() const;
    uint16_t getLoudnessValue() const;
    uint16_t getLoudnessRange() const;
    uint16_t getMaxTruePeakLevel() const;
    uint16_t getMaxMomentaryLoudness() const;
    uint16_t getMaxShortTermLoudness() const;
    const uint16_t *getReserved() const;
    vector<uint8_t> getCodingHistory() const;
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const BextChunk&chunk);

private:
    uint8_t Description[256]{};
    uint8_t Originator[32]{};
    uint8_t OriginatorReference[32]{};
    uint8_t OriginationDate[10]{};
    uint8_t OriginationTime[8]{};
    uint32_t TimeReferenceLow;
    uint32_t TimeReferenceHigh;
    uint16_t Version;
    uint8_t Umid[64]{};
    uint16_t LoudnessValue;
    uint16_t LoudnessRange;
    uint16_t MaxTruePeakLevel;
    uint16_t MaxMomentaryLoudness;
    uint16_t MaxShortTermLoudness;
    uint16_t Reserved[180]{};
    std::vector<uint8_t> CodingHistory;
};


#endif //MP3ENCODER_BEXTCHUNK_H
