#ifndef WAVEPP_WAVFILE_H
#define WAVEPP_WAVFILE_H

#include <cstdint>
#include <string>
#include <array>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include <RiffChunk.h>
#include <FmtChunk.h>
#include <DataChunk.h>
#include <CueChunk.h>
#include <BextChunk.h>
#include <FactChunk.h>
#include <PlaylistChunk.h>
#include <AssociatedDataListChunk.h>
#include <InstrumentChunk.h>

using namespace std;

class BaseWaveFile {

public:
    BaseWaveFile();
    explicit BaseWaveFile(const std::string& wav_file_path);
    virtual ~BaseWaveFile();
    RiffChunk *getRIFF() const;
    FmtChunk *getFmt() const;
    CueChunk *getCue() const;
    DataChunk *getData() const;
    void setRIFF(RiffChunk *RIFF);
    void setFmt(FmtChunk *Fmt);
    void setCue(CueChunk *Cue);
    void setData(DataChunk *Data);
    void setBext(BextChunk *Bext);
    BextChunk *getBext() const;
    FactChunk *getFact() const;
    void setFact(FactChunk *Fact);
    PlaylistChunk *getPlst() const;
    void setPlst(PlaylistChunk *Plst);
    AssociatedDataListChunk *getAdtl() const;
    void setAdtl(AssociatedDataListChunk *Adtl);
    InstrumentChunk *getInst() const;
    void setInst(InstrumentChunk *Inst);
    virtual bool checkFile();
    uint32_t calculateRIFFCkSize();
    const string &getFileName() const;
    void setFileName(const string &FileName);
    virtual int LoadFromFile(const std::string &wav_file_path);
    virtual void WriteToFile(const std::string &wav_file_path);
    virtual void WriteToFile();
    friend std::ostream& operator<<(std::ostream&stream, const BaseWaveFile&chunk);
    const string &getFileExtension() const;
    void setFileExtension(const string &fileExtension);
    const string &getFileFullPath() const;
    void setFileFullPath(const string &fileFullPath);
    RiffChunk *getRiff() const;
    void setRiff(RiffChunk *riff);
    void writeInfo();
    const vector<uint8_t> &getUnmappedChunks() const;
    void setUnmappedChunks(const vector<uint8_t> &unmappedChunks);

protected:
    string FileName;
    string FileExtension;
    string FileFullPath;
    RiffChunk *RIFF{};
    FmtChunk *Fmt{};
    CueChunk *Cue{};
    DataChunk *Data{};
    BextChunk *Bext{};
    FactChunk *Fact{};
    PlaylistChunk *Plst{};
    AssociatedDataListChunk *Adtl = nullptr;
    InstrumentChunk *Inst = nullptr;
    vector<uint8_t> unmappedChunks;
    bool checkRIFFIntegrity();
    virtual bool checkFmtIntegrity() { return false;};
    void parseFileFullPath(const std::string &wav_file_path);
};


#endif //WAVEPP_WAVFILE_H
