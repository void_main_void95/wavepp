#ifndef WAVEPP_LABELEDTEXTCHUNK_H
#define WAVEPP_LABELEDTEXTCHUNK_H

#include <BaseAssociatedDataListMember.h>

#define DW_SAMPLE_LENGTH_OFFSET 12
#define DW_PURPOSE_OFFSET 16
#define W_COUNTRY_OFFSET 20
#define W_LANGUAGE_OFFSET 22
#define W_DIALECT_OFFSET 24
#define W_CODE_PAG_OFFSET 26

using namespace std;

class LabeledTextChunk : public BaseAssociatedDataListMember{

public:
    LabeledTextChunk();
    explicit LabeledTextChunk(const uint8_t *RaWData);
    ~LabeledTextChunk() override;
    uint32_t getDwPurpose() const;
    void setDwPurpose(uint32_t dwPurpose);
    uint16_t getWCountry() const;
    void setWCountry(uint16_t wCountry);
    uint16_t getWLanguage() const;
    void setWLanguage(uint16_t wLanguage);
    uint16_t getWDialect() const;
    void setWDialect(uint16_t wDialect);
    uint16_t getWCodePage() const;
    void setWCodePage(uint16_t wCodePage);
    uint32_t getDwSampleLength() const;
    void setDwSampleLength(uint32_t dwSampleLength);
    int getRaw(vector<uint8_t> *raw) override;
    friend std::ostream& operator<<(std::ostream&stream, const LabeledTextChunk&chunk);

private:
    uint32_t dwSampleLength;
    uint32_t dwPurpose;
    uint16_t wCountry;
    uint16_t wLanguage;
    uint16_t wDialect;
    uint16_t wCodePage;
};


#endif //WAVEPP_LABELEDTEXTCHUNK_H

/// Questa classe deve essere controllata bene in relazione alla sua classe padre