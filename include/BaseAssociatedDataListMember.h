#ifndef WAVEPP_BASEASSOCIATEDDATALISTMEMBER_H
#define WAVEPP_BASEASSOCIATEDDATALISTMEMBER_H

/*this chunk may be not word aligned
 This object must handle padding byte for read and write
 */

#include <vector>
#include <cstring>
#include <BaseChunk.h>

#define BASE_DW_NAME_OFFSET 8
#define BASE_DATA_OFFSET 12
#define EXTENDED_DATA_OFFSET 28
#define EXTENDED_SIZE 16

using namespace std;

class BaseAssociatedDataListMember : public BaseChunk{

public:
    BaseAssociatedDataListMember();
    explicit BaseAssociatedDataListMember(const uint8_t *RaWData);
    ~BaseAssociatedDataListMember() override;
    uint32_t getDwName() const;
    void setDwName(uint32_t dwName);
    vector<uint8_t> getData() const;
    void setData(vector<uint8_t> &data);
    int getRaw(vector<uint8_t> *raw) override;
    const uint32_t getRawSize() const override;
    friend std::ostream& operator<<(std::ostream&stream, const BaseAssociatedDataListMember&chunk);

private:
   uint32_t dwName;
   vector<uint8_t> data;
};


#endif //WAVEPP_LABELCHUNK_H
